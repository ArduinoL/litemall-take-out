package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;

import org.linlinjava.litemall.db.dao.LitemallAdminMapper;
import org.linlinjava.litemall.db.dao.LitemallShopMapper;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallShopExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class LitemallShopService {
    @Resource
    private LitemallShopMapper shopMapper;
    @Autowired
    private LitemallAdminMapper adminMapper;
    
    @Transactional
    public int add(LitemallShop shop,LitemallAdmin admin) {
    	shop.setAddTime(LocalDateTime.now());
    	shop.setUpdateTime(LocalDateTime.now());
    	shop.setAdminId(admin.getId());
    	Integer result = shopMapper.insertSelective(shop);
    	if(result>0) {
    		//修改admin表对应信息
    		admin.setShopId(shop.getId());
    		adminMapper.updateByPrimaryKeySelective(admin);
    	}
        return result;
    }
    
    @Transactional
    public int add(LitemallShop shop) {
    	shop.setAddTime(LocalDateTime.now());
    	shop.setUpdateTime(LocalDateTime.now());
        return shopMapper.insertSelective(shop);
    }
    
    public List<LitemallShop> querySelective(String name, String shopPhone, String shopName, Integer page, Integer limit) {
        LitemallShopExample example = new LitemallShopExample();
        LitemallShopExample.Criteria criteria = example.createCriteria();
        if(!StringUtils.isEmpty(name)) {
        	criteria.andNameLike("%" + name + "%");
        }
        if(!StringUtils.isEmpty(shopPhone)) {
        	criteria.andShopPhoneLike("%" + shopPhone + "%");
        }
        if(!StringUtils.isEmpty(shopName)) {
        	criteria.andShopNameLike("%" + shopName + "%");
        }
        criteria.andDeletedEqualTo(false);

        PageHelper.startPage(page, limit);
        return shopMapper.selectByExample(example);
    }
    
    public List<LitemallShop> merchant_querySelective(LitemallAdmin admin, String name, String shopPhone, String shopName, Integer page, Integer limit) {
        LitemallShopExample example = new LitemallShopExample();
        LitemallShopExample.Criteria criteria = example.createCriteria();
        
        if(admin == null) {
        	return null;
        }
        	criteria.andAdminIdEqualTo(admin.getId());
        if(!StringUtils.isEmpty(name)) {
        	criteria.andNameLike("%" + name + "%");
        }
        if(!StringUtils.isEmpty(shopPhone)) {
        	criteria.andShopPhoneLike("%" + shopPhone + "%");
        }
        if(!StringUtils.isEmpty(shopName)) {
        	criteria.andShopNameLike("%" + shopName + "%");
        }
        criteria.andDeletedEqualTo(false);

        PageHelper.startPage(page, limit);
        return shopMapper.selectByExample(example);
    }
    
    public int updateById(LitemallShop shop) {
    	shop.setUpdateTime(LocalDateTime.now());
    	return shopMapper.updateByPrimaryKeySelective(shop);
    }
    
    public int delShop(LitemallShop shop) {
    	shop.setDeleted(true);
    	shop.setUpdateTime(LocalDateTime.now());
    	return shopMapper.updateByPrimaryKey(shop);
    }
    
    public List<LitemallShop> selectShop(){
    	return shopMapper.selectShop();
    }
    
    public String[] selectShopIds(String[] ShopIds){
    	List<LitemallShop> listShop = shopMapper.selectShopIds(ShopIds);
    	String[] shopName = new String[listShop==null?0:listShop.size()];
    	if(listShop != null) {
    		for(int i=0;i<listShop.size();i++) {
    			shopName[i] = listShop.get(i).getShopName();
    		}
    	}
    	return shopName;
    }
    
    public List<LitemallShop> selectlistShop(String[] ShopIds){
    	return shopMapper.selectShopIds(ShopIds);
    }
    
    public List<LitemallShop> selectShoplist(String[] ShopIds){
    	if(ShopIds.length>0) {
    		return shopMapper.selectShoplist(ShopIds);
    	}
    	return null;
    }
    
    public List<LitemallShop> queryByListShopId(List<Integer> shopIdList){
    	return shopMapper.queryByListShopId(shopIdList);
    }
    
    public LitemallShop getOneByShopID(Integer shopId) {
    	return shopMapper.selectByPrimaryKey(shopId);
    }
    
    
    public LitemallShop getShopByAdminID(Integer adminId) {
    	return shopMapper.getShopByAdminID(adminId);
    }
    
    public List<LitemallShop> getShopList(){
    	LitemallShopExample example = new LitemallShopExample();
    	return shopMapper.selectByExample(example);
    }
    
    public List<LitemallShop> getByListShopId(List<Integer> shopIdList){
    	return shopMapper.getByListShopId(shopIdList);
    }
  /*  
    public List<LitemallShop> get_lng_lat(String lng, String lat,Integer page, Integer limit) {

        if (StringUtils.isEmpty(lng)) {
            return null;
        }
        if (StringUtils.isEmpty(lat)) {
        	return null;
        }

        return shopMapper.get_lng_lat(lng,lat,page,limit);
    }
    
    public LitemallShop getDistance(String lng, String lat) {
    	return shopMapper.getDistance(lng, lat);
    }
    
    public int updateById(LitemallShop shop) {
    	shop.setUpdateTime(LocalDateTime.now());
        return shopMapper.updateByPrimaryKeySelective(shop);
    }
    
    public int delShop(Integer id) {
    	return shopMapper.delShop(id);
    }

    public LitemallShop getOneByShopID(Integer id) {
    	return shopMapper.getOneByShopID(id);
    }
    
    public List<LitemallShop> queryByListShopId(List<Integer> shopIdList){
    	return shopMapper.queryByListShopId(shopIdList);
    }
    
    
    public LitemallShop getAgentGoodsIds(Integer userId){
    	return shopMapper.getAgentGoodsIds(userId);
    }
    
    public int insertAgentGoods(LitemallShop agentGoods) {
    	return shopMapper.insertAgentGoods(agentGoods);
    }
    
    public int updateByuserId(LitemallShop userId) {
    	return shopMapper.updateByuserId(userId);
    }*/
}
