package org.linlinjava.litemall.db.domain;

import java.time.LocalDateTime;

public class LitemallActiveUser {

	private Integer id;
	
	private Integer users;
	
	private LocalDateTime addTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUsers() {
		return users;
	}

	public void setUsers(Integer users) {
		this.users = users;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	@Override
	public String toString() {
		return "LitemallActiveUser [id=" + id + ", users=" + users + ", addTime=" + addTime + "]";
	}
	
	
}
