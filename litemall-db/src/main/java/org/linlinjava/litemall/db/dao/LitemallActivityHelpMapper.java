package org.linlinjava.litemall.db.dao;


import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallActivityHelp;

public interface LitemallActivityHelpMapper {
    
	//LitemallActivity getActivityById(Integer id);
    
    int insertActivityHelp(LitemallActivityHelp ayhelp);
    
    List<LitemallActivityHelp> getNumBerByactId(Integer actId);
    
    LitemallActivityHelp getCountByUserIdAndActId(@Param("userId") Integer userId,@Param("actId") Integer actId);
    
    List<LitemallActivityHelp> getCountByUserId(Integer userId);
}