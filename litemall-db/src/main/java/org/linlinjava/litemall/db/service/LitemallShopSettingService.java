package org.linlinjava.litemall.db.service;

import java.time.LocalDateTime;

import org.linlinjava.litemall.db.dao.LitemallShopBalanceMapper;
import org.linlinjava.litemall.db.dao.LitemallShopMapper;
import org.linlinjava.litemall.db.dao.LitemallShopSettingMapper;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallShopBalance;
import org.linlinjava.litemall.db.domain.LitemallShopSetting;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class LitemallShopSettingService {
	
    @Autowired
    private LitemallShopSettingMapper shopSettingMapper;
    @Autowired
    private LitemallShopMapper shopMapper;
    @Autowired
    private LitemallShopBalanceMapper shopBalanceMapper;
    
    public LitemallShopSetting getOneByShopId(Integer ShopId) {
    	return shopSettingMapper.getOneByShopId(ShopId);
    }
    
    @Transactional
    public int add(LitemallShopSetting ShopSetting) {
    	ShopSetting.setAddTime(LocalDateTime.now());
    	ShopSetting.setUpdateTime(LocalDateTime.now());
    	int result = shopSettingMapper.insertSelective(ShopSetting);
    	if(result>0) {
    		//修改 litemall_shop 表相关信息
    		LitemallShop shop = new LitemallShop();
    		shop.setId(ShopSetting.getShopId());
    		shop.setDeliveryPrice(ShopSetting.getDeliveryPrice());
    		shop.setInitialPrice(ShopSetting.getInitialPrice());
    		int r = shopMapper.updateByPrimaryKeySelective(shop);
    		if(r>0) {
    			//创建店铺余额表
    			LitemallShopBalance shopBalance = new LitemallShopBalance();
    			shopBalance.setShopId(ShopSetting.getShopId());
    			shopBalance.setRate(ShopSetting.getRate());
    			shopBalance.setAddTime(LocalDateTime.now());
    			shopBalance.setUpdateTime(LocalDateTime.now());
    			shopBalanceMapper.insertSelective(shopBalance);
    		}
    	}
    	return result;
    }
    
    @Transactional
    public int update(LitemallShopSetting ShopSetting) {
    	ShopSetting.setUpdateTime(LocalDateTime.now());
    	int result = shopSettingMapper.updateByPrimaryKeySelective(ShopSetting);
    	if(result>0) {
    		//更新 litemall_shop 表相关信息
    		LitemallShop shop = new LitemallShop();
    		shop.setId(ShopSetting.getShopId());
    		shop.setDeliveryPrice(ShopSetting.getDeliveryPrice());
    		shop.setInitialPrice(ShopSetting.getInitialPrice());
    		int r = shopMapper.updateByPrimaryKeySelective(shop);
    		if(r>0) {
    			//更新 litemall_shop_balance 表相关信息
    			LitemallShopBalance shopBalance = new LitemallShopBalance();
    			shopBalance.setShopId(ShopSetting.getShopId());
    			shopBalance.setRate(ShopSetting.getRate());
    			shopBalance.setUpdateTime(LocalDateTime.now());
    			shopBalanceMapper.updateByShopId(shopBalance);
    		}
    	}
    	return result;
    }
    
    public LitemallShopSetting getOneSetting(Integer ShopId) {
    	return shopSettingMapper.getOneByShopId(ShopId);
    }
    
    
}
