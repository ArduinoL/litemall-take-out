package org.linlinjava.litemall.db.domain;

import java.time.LocalDateTime;

public class LitemallActivity {

	private Integer id;
	
	private Integer userId;
	
	private Integer status;
	
	private String name;
	
	private Integer number;
	
	private Integer addNumber;
	
    private LocalDateTime addTime;

    private LocalDateTime updateTime;
    
    private Boolean deleted;
    
    private String avatar;
    
    private String nickname;
    
    private String shareUrl;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getAddNumber() {
		return addNumber;
	}

	public void setAddNumber(Integer addNumber) {
		this.addNumber = addNumber;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	public Boolean getDeleted() {
		return deleted;
	}

	public void setDeleted(Boolean deleted) {
		this.deleted = deleted;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getNickname() {
		return nickname;
	}

	public void setNickname(String nickname) {
		this.nickname = nickname;
	}

	public String getShareUrl() {
		return shareUrl;
	}

	public void setShareUrl(String shareUrl) {
		this.shareUrl = shareUrl;
	}

	@Override
	public String toString() {
		return "LitemallActivity [id=" + id + ", userId=" + userId + ", status=" + status + ", name=" + name
				+ ", number=" + number + ", addNumber=" + addNumber + ", addTime=" + addTime + ", updateTime="
				+ updateTime + ", deleted=" + deleted + ", avatar=" + avatar + ", nickname=" + nickname + ", shareUrl="
				+ shareUrl + "]";
	}
    
    
    
}
