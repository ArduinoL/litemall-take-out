package org.linlinjava.litemall.db.service;


import java.time.LocalDateTime;
import java.util.List;
import org.linlinjava.litemall.db.dao.LitemallShopBalanceLogMapper;
import org.linlinjava.litemall.db.dao.LitemallShopBalanceMapper;
import org.linlinjava.litemall.db.domain.LitemallShopBalance;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceLog;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceLogExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
public class LitemallShopBalanceLogService {
	
    @Autowired
    private LitemallShopBalanceLogMapper shopBalanceLogMapper;
    @Autowired
    private LitemallShopBalanceMapper shopBalanceMapper;
    
    
    public List<LitemallShopBalanceLog> querySelective(){
    	LitemallShopBalanceLogExample example = new LitemallShopBalanceLogExample();
    	LitemallShopBalanceLogExample.Criteria criteria = example.createCriteria();
    	criteria.andStatusEqualTo(0);
    	return shopBalanceLogMapper.selectByExample(example);
    }
    
    @Transactional
    public int updateShopBalanceLogANDBalance(LitemallShopBalanceLog record) {
    	record.setStatus(1);
    	record.setUpdateTime(LocalDateTime.now());
    	int res = 0; 
    	int result = shopBalanceLogMapper.updateByPrimaryKeySelective(record);
    	if(result == 1) {
    		//修改商户余额
    		LitemallShopBalance be = new LitemallShopBalance();
    		be.setShopId(record.getShopId());
    		be.setUpdateTime(LocalDateTime.now());
    		be.setBalance(record.getActualPrice());
    		res = shopBalanceMapper.updateShopBalanceByShopId(be);
    	}
    	return res;
    }
    
    public LitemallShopBalanceLog getOneShopBalanceLogByorderId(Integer orderId) {
    	return shopBalanceLogMapper.getOneByOrderId(orderId);
    }
    
    public int updateShopBalanceLog(LitemallShopBalanceLog record) {
    	record.setUpdateTime(LocalDateTime.now());
    	return shopBalanceLogMapper.updateByPrimaryKeySelective(record);
    }
    
}
