package org.linlinjava.litemall.db.dao;


import java.util.List;

import org.linlinjava.litemall.db.domain.LitemallUserBalance;

public interface LitemallUserBalanceMapper {

	LitemallUserBalance getUserBalance(Integer shopId);
	
	List<LitemallUserBalance> getShopIncomeList();
	
	int insertUserBalance(LitemallUserBalance userBalance);
	
	int updateUserBalance(LitemallUserBalance userBalance);
}