package org.linlinjava.litemall.db.domain;

import java.time.LocalDateTime;

public class LitemallActivityHelp {

	private Integer id;
	
	private Integer actId;
	
	private Integer userId;
	
	private String avatar;
	
    private LocalDateTime addTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getActId() {
		return actId;
	}

	public void setActId(Integer actId) {
		this.actId = actId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	@Override
	public String toString() {
		return "LitemallActivityHelp [id=" + id + ", actId=" + actId + ", userId=" + userId + ", avatar=" + avatar
				+ ", addTime=" + addTime + "]";
	}

    
    
}
