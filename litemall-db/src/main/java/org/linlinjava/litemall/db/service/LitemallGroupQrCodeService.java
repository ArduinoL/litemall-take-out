package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallGroupQrcodeMapper;
import org.linlinjava.litemall.db.domain.LitemallGroupQrcode;
import org.linlinjava.litemall.db.domain.LitemallGroupQrcodeExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class LitemallGroupQrCodeService {
    @Autowired
    private LitemallGroupQrcodeMapper GroupQrcodeMapper;


    public List<LitemallGroupQrcode> querySelective(Integer page, Integer limit) {
        LitemallGroupQrcodeExample example = new LitemallGroupQrcodeExample();
        LitemallGroupQrcodeExample.Criteria criteria = example.createCriteria();

        /*
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        if (!StringUtils.isEmpty(content)) {
            criteria.andContentLike("%" + content + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }*/

        PageHelper.startPage(page, limit);
        return GroupQrcodeMapper.selectByExample(example);
    }

    public int updateById(LitemallGroupQrcode groupQrcode) {
        return GroupQrcodeMapper.updateByPrimaryKeySelective(groupQrcode);
    }

    public void deleteById(Integer id) {
    	GroupQrcodeMapper.deleteByPrimaryKey(id);
    }

    public void add(LitemallGroupQrcode groupQrcode) {
    	groupQrcode.setPid(0);
    	groupQrcode.setType(0);
    	groupQrcode.setAddTime(LocalDateTime.now());
    	groupQrcode.setExpireTime(LocalDateTime.now().plusDays(7));//过期时间 默认7 天
    	GroupQrcodeMapper.insertSelective(groupQrcode);
    }

    public LitemallGroupQrcode findById(Integer id) {
        return GroupQrcodeMapper.selectByPrimaryKey(id);
    }
    
    public LitemallGroupQrcode findByUUID(String uuid) {
    	return GroupQrcodeMapper.findByUUID(uuid);
    }
}
