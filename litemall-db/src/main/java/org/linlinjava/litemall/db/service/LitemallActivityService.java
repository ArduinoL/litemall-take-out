package org.linlinjava.litemall.db.service;


import java.time.LocalDateTime;

import org.linlinjava.litemall.db.dao.LitemallActivityMapper;
import org.linlinjava.litemall.db.domain.LitemallActivity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LitemallActivityService {
    @Autowired
    private LitemallActivityMapper activityMapper;
    
    public LitemallActivity getActivityById(Integer id){
    	return activityMapper.getActivityById(id);
    }
    
    public LitemallActivity getActivityByUserId(Integer userId){
    	return activityMapper.getActivityByUserId(userId);
    }
    
    public int insertActivity(LitemallActivity ay) {
    	ay.setAddTime(LocalDateTime.now());
    	ay.setUpdateTime(LocalDateTime.now());
    	return activityMapper.insertActivity(ay);
    }
    
    public int updateById(LitemallActivity ay) {
    	ay.setUpdateTime(LocalDateTime.now());
    	return activityMapper.updateById(ay);
    }
    
    public Integer getActivityByUserIdCount(Integer userId) {
    	return (int) activityMapper.getActivityByUserIdCount(userId);
    }
    
    public Integer getGoodsA(Integer userId) {
    	return (int) activityMapper.getGoodsA(userId);
    }
    
    public LitemallActivity getOneGift(Integer userId) {
    	return activityMapper.getOneGift(userId);
    }

}
