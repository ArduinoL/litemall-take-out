package org.linlinjava.litemall.db.service;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.linlinjava.litemall.db.dao.LitemallActiveUserMapper;
import org.linlinjava.litemall.db.domain.LitemallActiveUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LitemallActiveUserService {
    @Autowired
    private LitemallActiveUserMapper ActiveUseryMapper;
    
    public int insertActiveUser(LitemallActiveUser ar) {
    	ar.setAddTime(LocalDateTime.now().minusDays(1));
    	return ActiveUseryMapper.insertActiveUser(ar);
    }
    
    public List<Map> getActiveUser(){
    	return ActiveUseryMapper.getActiveUser();
    }
}
