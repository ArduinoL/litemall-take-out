package org.linlinjava.litemall.db.service;


import org.linlinjava.litemall.db.dao.WxTemplateMsgLogMapper;
import org.linlinjava.litemall.db.domain.WxTemplateMsgLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class WxTemplateMsgLogService {
	
	@Autowired
	private WxTemplateMsgLogMapper wxTemplateMsgLogMapper;

    public int saveLog(WxTemplateMsgLog templateLog) {
        return wxTemplateMsgLogMapper.insertSelective(templateLog);
    }
    
    
}
