package org.linlinjava.litemall.db.service;


import java.time.LocalDateTime;

import org.linlinjava.litemall.db.dao.LitemallAgentGoodsMapper;
import org.linlinjava.litemall.db.domain.LitemallAgentGoods;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LitemallAgentGoodsService {
    @Autowired
    private LitemallAgentGoodsMapper agentGoodsMapper;
    
    public LitemallAgentGoods getAgentGoodsIds(Integer userId){
    	return agentGoodsMapper.getAgentGoodsIds(userId);
    }
    
    public int insertAgentGoods(LitemallAgentGoods agentGoods) {
    	agentGoods.setAddTime(LocalDateTime.now());
    	agentGoods.setUpdateTime(LocalDateTime.now());
    	return agentGoodsMapper.insertAgentGoods(agentGoods);
    }
    
    public int updateById(LitemallAgentGoods agentGoods) {
    	agentGoods.setUpdateTime(LocalDateTime.now());
    	return agentGoodsMapper.updateById(agentGoods);
    }

}
