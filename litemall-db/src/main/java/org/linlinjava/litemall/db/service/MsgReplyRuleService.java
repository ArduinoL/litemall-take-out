package org.linlinjava.litemall.db.service;

import java.util.List;

import org.linlinjava.litemall.db.dao.WxUserMapper;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.domain.LitemallUserExample;
import org.linlinjava.litemall.db.domain.WxUser;
import org.linlinjava.litemall.db.domain.WxUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;

@Service
public class MsgReplyRuleService {
	
	@Autowired
	private WxUserMapper wxUserMapper;

    public List<WxUser> querySelective(Integer page, Integer size) {
		WxUserExample example = new WxUserExample();
		WxUserExample.Criteria criteria = example.createCriteria();
		
        PageHelper.startPage(page, size);
        return wxUserMapper.selectByExample(example);
    }
}
