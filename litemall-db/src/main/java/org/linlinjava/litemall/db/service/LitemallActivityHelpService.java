package org.linlinjava.litemall.db.service;


import java.time.LocalDateTime;
import java.util.List;

import org.linlinjava.litemall.db.dao.LitemallActivityHelpMapper;
import org.linlinjava.litemall.db.domain.LitemallActivityHelp;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LitemallActivityHelpService {
    @Autowired
    private LitemallActivityHelpMapper activityHelpMapper;
    
    public int insertActivity(LitemallActivityHelp ay) {
    	ay.setAddTime(LocalDateTime.now());
    	return activityHelpMapper.insertActivityHelp(ay);
    }
    
    public List<LitemallActivityHelp> getNumBerByactId(Integer actId) {
    	return activityHelpMapper.getNumBerByactId(actId);
    }
    
    public LitemallActivityHelp getCountByUserIdAndActId(Integer userId,Integer actId) {
    	return activityHelpMapper.getCountByUserIdAndActId(userId,actId);
    }
    
    public List<LitemallActivityHelp> getCountByUserId(Integer userId) {
    	return activityHelpMapper.getCountByUserId(userId);
    }

}
