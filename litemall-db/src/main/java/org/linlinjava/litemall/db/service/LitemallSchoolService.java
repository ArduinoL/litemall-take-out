package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;

import org.linlinjava.litemall.db.dao.LitemallAdminMapper;
import org.linlinjava.litemall.db.dao.LitemallSchoolMapper;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.domain.LitemallSchool;
import org.linlinjava.litemall.db.domain.LitemallSchoolExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class LitemallSchoolService {
    @Resource
    private LitemallSchoolMapper schoolMapper;
    @Resource
    private LitemallAdminMapper adminMapper;
    
    public List<LitemallSchool> get_lng_lat(String longitude,String latitude){
    	return null;
    }
    
    public int add(LitemallSchool school) {
    	school.setAddTime(LocalDateTime.now());
    	school.setUpdateTime(LocalDateTime.now());
        return schoolMapper.insertSelective(school);
    }
    
    public List<LitemallSchool> querySelective(String SchoolName,Integer page, Integer limit) {
        LitemallSchoolExample example = new LitemallSchoolExample();
        LitemallSchoolExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(SchoolName)) {
            criteria.andSchoolNameLike("%" + SchoolName + "%");
        }

        PageHelper.startPage(page, limit);
        return schoolMapper.selectByExample(example);
    }
    
    public LitemallSchool getOneSchool(Integer SchoolId) {
    	return schoolMapper.selectByPrimaryKey(SchoolId);
    }
    
    public int updateById(LitemallSchool school) {
    	school.setUpdateTime(LocalDateTime.now());
        return schoolMapper.updateByPrimaryKeySelective(school);
    }
    
    public List<LitemallAdmin> getlistAdmin() {
    	return adminMapper.selectAll();
    }
    
    public List<LitemallSchool> get_lng_lat(String lng, String lat,Integer page, Integer limit) {

        if (StringUtils.isEmpty(lng)) {
            return null;
        }
        if (StringUtils.isEmpty(lat)) {
        	return null;
        }
        return schoolMapper.get_lng_lat(lng,lat,page,limit);
    }
}
