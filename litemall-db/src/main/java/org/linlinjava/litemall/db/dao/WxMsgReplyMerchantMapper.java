package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.WxMsgReplyMerchant;
import org.linlinjava.litemall.db.domain.WxMsgReplyMerchantExample;

public interface WxMsgReplyMerchantMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    long countByExample(WxMsgReplyMerchantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int deleteByExample(WxMsgReplyMerchantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int insert(WxMsgReplyMerchant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int insertSelective(WxMsgReplyMerchant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    WxMsgReplyMerchant selectOneByExample(WxMsgReplyMerchantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    WxMsgReplyMerchant selectOneByExampleSelective(@Param("example") WxMsgReplyMerchantExample example, @Param("selective") WxMsgReplyMerchant.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    List<WxMsgReplyMerchant> selectByExampleSelective(@Param("example") WxMsgReplyMerchantExample example, @Param("selective") WxMsgReplyMerchant.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    List<WxMsgReplyMerchant> selectByExample(WxMsgReplyMerchantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    WxMsgReplyMerchant selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") WxMsgReplyMerchant.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    WxMsgReplyMerchant selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") WxMsgReplyMerchant record, @Param("example") WxMsgReplyMerchantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") WxMsgReplyMerchant record, @Param("example") WxMsgReplyMerchantExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(WxMsgReplyMerchant record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table wx_msg_reply_merchant
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(WxMsgReplyMerchant record);
}