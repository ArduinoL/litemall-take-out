package org.linlinjava.litemall.db.domain;

import java.time.LocalDateTime;

public class LitemallAgentGoods {

	private Integer id;
	
	private Integer userId;
	
	private String productIds;
	
    private LocalDateTime addTime;
    
    private LocalDateTime updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getProductIds() {
		return productIds;
	}

	public void setProductIds(String productIds) {
		this.productIds = productIds;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "LitemallAgentGoods [id=" + id + ", userId=" + userId + ", productIds=" + productIds + ", addTime="
				+ addTime + ", updateTime=" + updateTime + "]";
	}

    
}
