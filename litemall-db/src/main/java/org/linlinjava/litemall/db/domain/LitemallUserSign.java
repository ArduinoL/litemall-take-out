package org.linlinjava.litemall.db.domain;

import java.time.LocalDateTime;

public class LitemallUserSign {

	private Integer id;
	
	private Integer userId;
	
	private String record;
	
	private Integer stage;
	
	private Integer number;
	
	private Integer gain;
	
	private Integer have;
	
	private LocalDateTime addTime;
	
	private LocalDateTime updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getRecord() {
		return record;
	}

	public void setRecord(String record) {
		this.record = record;
	}

	public Integer getStage() {
		return stage;
	}

	public void setStage(Integer stage) {
		this.stage = stage;
	}

	public Integer getNumber() {
		return number;
	}

	public void setNumber(Integer number) {
		this.number = number;
	}

	public Integer getGain() {
		return gain;
	}

	public void setGain(Integer gain) {
		this.gain = gain;
	}

	public Integer getHave() {
		return have;
	}

	public void setHave(Integer have) {
		this.have = have;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "LitemallUserSign [id=" + id + ", userId=" + userId + ", record=" + record + ", stage=" + stage
				+ ", number=" + number + ", gain=" + gain + ", have=" + have + ", addTime=" + addTime + ", updateTime="
				+ updateTime + "]";
	}
	
	
}
