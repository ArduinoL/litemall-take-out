package org.linlinjava.litemall.db.service;


import java.time.LocalDateTime;

import org.linlinjava.litemall.db.dao.LitemallUserSignMapper;
import org.linlinjava.litemall.db.domain.LitemallUserSign;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LitemallUserSignService {
    @Autowired
    private LitemallUserSignMapper userSignMapper;
    
    public LitemallUserSign getOneSignUser(Integer userId) {
    	return userSignMapper.getOneSignUser(userId);
    }
    
    public int updateByUserId(LitemallUserSign us) {
    	us.setUpdateTime(LocalDateTime.now());
    	return userSignMapper.updateByUserId(us);
    }
    
    public int insertUserSign(LitemallUserSign us) {
    	//us.setAddTime(LocalDateTime.now());
    	us.setUpdateTime(LocalDateTime.now());
    	return userSignMapper.insertUserSign(us);
    }
    
}
