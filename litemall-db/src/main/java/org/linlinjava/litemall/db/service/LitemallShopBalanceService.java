package org.linlinjava.litemall.db.service;


import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.linlinjava.litemall.db.dao.LitemallShopBalanceMapper;
import org.linlinjava.litemall.db.dao.LitemallShopMapper;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallShopBalance;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceExample;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceLog;
import org.linlinjava.litemall.db.domain.LitemallShopExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.github.pagehelper.PageHelper;


@Service
public class LitemallShopBalanceService {
	
    @Autowired
    private LitemallShopBalanceMapper shopBalanceMapper;
    @Autowired
    private LitemallShopMapper shopMapper;
    
    public List<LitemallShopBalance> querySelective(String name, String shopPhone, String shopName, Integer page, Integer limit) {
    	LitemallShopBalanceExample example = new LitemallShopBalanceExample();
    	LitemallShopBalanceExample.Criteria criteria = example.createCriteria();
    	/*
        if(!StringUtils.isEmpty(name)) {
        	criteria.andNameLike("%" + name + "%");
        }
        if(!StringUtils.isEmpty(shopPhone)) {
        	criteria.andShopPhoneLike("%" + shopPhone + "%");
        }
        if(!StringUtils.isEmpty(shopName)) {
        	criteria.andShopNameLike("%" + shopName + "%");
        }*/

        PageHelper.startPage(page, limit);
        List<LitemallShopBalance> shopBalanceList = shopBalanceMapper.selectByExample(example);
        if(shopBalanceList.size()>0) {
            List<Integer> shopIdList = shopBalanceList.stream().map(LitemallShopBalance::getShopId).collect(Collectors.toList());
            List<LitemallShop> shopList = shopMapper.getByListShopId(shopIdList);
            for(int i=0;i<shopBalanceList.size();i++) {
            	for(int j=0;j<shopList.size();j++) {
            		if(shopBalanceList.get(i).getShopId().equals(shopList.get(j).getId())) {
            			shopBalanceList.get(i).setShopName(shopList.get(j).getShopName());
            		}
            	}
            }
        }

        return shopBalanceList;
    }
    
    
    public int updataShopBalance(LitemallShopBalanceLog shopBalanceLog) {
    	LitemallShopBalance shopBalance = shopBalanceMapper.selectBalanceByShopId(shopBalanceLog.getShopId());
    	BigDecimal balance = shopBalance.getBalance().subtract(shopBalanceLog.getActualPrice());
    	
    	LitemallShopBalance b = new LitemallShopBalance();
    	b.setId(shopBalance.getId());
    	b.setShopId(shopBalance.getShopId());
    	b.setRate(shopBalance.getRate());
    	b.setBalance(balance);
    	b.setNotCashing(shopBalance.getNotCashing());
    	b.setFreeze(shopBalance.getFreeze());
    	b.setCashingNum(shopBalance.getCashingNum());
    	b.setUpdateTime(LocalDateTime.now());
    	return shopBalanceMapper.updateByPrimaryKey(b);
    }
    
    
}
