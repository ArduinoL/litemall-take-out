package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallFootprintMapper;
import org.linlinjava.litemall.db.domain.LitemallFootprint;
import org.linlinjava.litemall.db.domain.LitemallFootprintExample;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class LitemallFootprintService {
    @Resource
    private LitemallFootprintMapper footprintMapper;
    @Autowired
    private LitemallUserService userService;
    @Autowired
    private LitemallGoodsService goodsService;

    public List<LitemallFootprint> queryByAddTime(Integer userId, Integer page, Integer size) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        example.or().andUserIdEqualTo(userId).andDeletedEqualTo(false);
        example.setOrderByClause(LitemallFootprint.Column.addTime.desc());
        PageHelper.startPage(page, size);
        return footprintMapper.selectByExample(example);
    }

    public LitemallFootprint findById(Integer id) {
        return footprintMapper.selectByPrimaryKey(id);
    }

    public LitemallFootprint findById(Integer userId, Integer id) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        example.or().andIdEqualTo(id).andUserIdEqualTo(userId).andDeletedEqualTo(false);
        return footprintMapper.selectOneByExample(example);
    }

    public void deleteById(Integer id) {
        footprintMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(LitemallFootprint footprint) {
        footprint.setAddTime(LocalDateTime.now());
        footprint.setUpdateTime(LocalDateTime.now());
        footprintMapper.insertSelective(footprint);
    }

    public List<LitemallFootprint> querySelective(String userId, String goodsId, Integer page, Integer size, String sort, String order) {
        LitemallFootprintExample example = new LitemallFootprintExample();
        LitemallFootprintExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(userId)) {
            criteria.andUserIdEqualTo(Integer.valueOf(userId));
        }
        if (!StringUtils.isEmpty(goodsId)) {
            criteria.andGoodsIdEqualTo(Integer.valueOf(goodsId));
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        List<LitemallFootprint> footprintList = footprintMapper.selectByExample(example);
        List<Integer> FootprintList = footprintList.stream().map(LitemallFootprint::getUserId).collect(Collectors.toList());
        List<Integer> goodsIdList = footprintList.stream().map(LitemallFootprint::getGoodsId).collect(Collectors.toList());
        List<LitemallUser> UserList = userService.queryByListUserId(FootprintList);
        List<LitemallGoods> goodsList = goodsService.queryByListGoodsId(goodsIdList);
        for(int i=0;i<footprintList.size();i++) {
        	int cId = footprintList.get(i).getUserId();
			for(int j=0;j<UserList.size();j++) { 
			int uId = UserList.get(j).getId();
			if(uId == cId) {
				footprintList.get(i).setNickname(UserList.get(j).getNickname());
				footprintList.get(i).setAvatar(UserList.get(j).getAvatar()); 
			   } 
			}
			
			int collecId = footprintList.get(i).getGoodsId();
        	for(int k=0;k<goodsList.size();k++) {
        		int goodsId2 = goodsList.get(k).getId();
        		if(goodsId2 == collecId) {
        			footprintList.get(i).setName(goodsList.get(k).getName());
        			footprintList.get(i).setPicUrl(goodsList.get(k).getPicUrl());
        		}
        	}
        }
        return footprintList;
    }
}
