package org.linlinjava.litemall.db.dao;

import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.SysPushMsg;
import org.linlinjava.litemall.db.domain.SysPushMsgExample;

public interface SysPushMsgMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    long countByExample(SysPushMsgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int deleteByExample(SysPushMsgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int deleteByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int insert(SysPushMsg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int insertSelective(SysPushMsg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    SysPushMsg selectOneByExample(SysPushMsgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    SysPushMsg selectOneByExampleSelective(@Param("example") SysPushMsgExample example, @Param("selective") SysPushMsg.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    List<SysPushMsg> selectByExampleSelective(@Param("example") SysPushMsgExample example, @Param("selective") SysPushMsg.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    List<SysPushMsg> selectByExample(SysPushMsgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    SysPushMsg selectByPrimaryKeySelective(@Param("id") Integer id, @Param("selective") SysPushMsg.Column ... selective);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    SysPushMsg selectByPrimaryKey(Integer id);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int updateByExampleSelective(@Param("record") SysPushMsg record, @Param("example") SysPushMsgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int updateByExample(@Param("record") SysPushMsg record, @Param("example") SysPushMsgExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int updateByPrimaryKeySelective(SysPushMsg record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table sys_push_msg
     *
     * @mbg.generated
     */
    int updateByPrimaryKey(SysPushMsg record);
}