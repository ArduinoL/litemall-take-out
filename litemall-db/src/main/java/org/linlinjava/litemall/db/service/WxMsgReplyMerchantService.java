package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.dao.WxMsgReplyMerchantMapper;
import org.linlinjava.litemall.db.domain.WxMsgReplyMerchant;
import org.linlinjava.litemall.db.domain.WxMsgReplyMerchantExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class WxMsgReplyMerchantService {
	
    @Autowired
    private WxMsgReplyMerchantMapper wxMsgReplyMerchantMapper;
    
    
    public WxMsgReplyMerchant findByShop(Integer shopId) {
    	WxMsgReplyMerchantExample example = new WxMsgReplyMerchantExample();
    	WxMsgReplyMerchantExample.Criteria criteria = example.createCriteria();
    	criteria.andShopIdEqualTo(shopId);
    	return wxMsgReplyMerchantMapper.selectOneByExample(example);
    }
    
    public int add(WxMsgReplyMerchant record){
    	WxMsgReplyMerchantExample example = new WxMsgReplyMerchantExample();
    	WxMsgReplyMerchantExample.Criteria criteria = example.createCriteria();
    	criteria.andShopIdEqualTo(record.getShopId());
    	WxMsgReplyMerchant wt = wxMsgReplyMerchantMapper.selectOneByExample(example);
    	if(wt != null) {
    		return 0;
    	}
    	return wxMsgReplyMerchantMapper.insertSelective(record);
    }

}
