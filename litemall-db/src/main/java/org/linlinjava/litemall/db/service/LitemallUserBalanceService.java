package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.dao.LitemallUserBalanceMapper;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.domain.LitemallUserBalance;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

@Service
public class LitemallUserBalanceService {
    @Resource
    private LitemallUserBalanceMapper userBalanceMapper;



    public LitemallUserBalance getUserBalance(Integer shopId) {
    	return userBalanceMapper.getUserBalance(shopId);
    }

    public List<LitemallUserBalance> getShopIncomeList(){
    	return userBalanceMapper.getShopIncomeList();
    }
    
    public int insertUserBalance(LitemallUser user) {
    	LitemallUserBalance userBalance = new LitemallUserBalance();
    	BigDecimal m = new BigDecimal(0.00);
    	userBalance.setUserId(user.getId());
    	userBalance.setShopId(user.getShopId());
    	userBalance.setYield(0.0);
    	userBalance.setType(1);//'结算类型：1按商品佣金，2用户特定收益率',
    	userBalance.setBalance(m);
    	userBalance.setNotCashing(m);
    	userBalance.setFreeze(m);
    	userBalance.setCashingNum(0);
    	userBalance.setAddTime(LocalDateTime.now());
    	userBalance.setUpdateTime(LocalDateTime.now());
    	//可能存在修改用户绑定门店，导致生成多条用户余额记录，绑定门店的用户不允许再修改门店信息，只能通过程序员操作。
    	return userBalanceMapper.insertUserBalance(userBalance);
    }
    
    public int updateUserBalance(LitemallUserBalance userBalance) {
    	return userBalanceMapper.updateUserBalance(userBalance);
    }
}
