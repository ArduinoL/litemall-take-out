package org.linlinjava.litemall.db.service;

import java.util.List;

import org.linlinjava.litemall.db.dao.WxMsgReplyUserMapper;
import org.linlinjava.litemall.db.domain.WxMsgReplyUser;
import org.linlinjava.litemall.db.domain.WxMsgReplyUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;


@Service
public class MsgReplyUserService {
	
	@Autowired
	private WxMsgReplyUserMapper wxMsgReplyUserMapper;

    public List<WxMsgReplyUser> querySelective(String TemplateType) {
    	WxMsgReplyUserExample example = new WxMsgReplyUserExample();
    	WxMsgReplyUserExample.Criteria criteria = example.createCriteria();
    	
    	if(!StringUtils.isEmpty(TemplateType)) {
    		criteria.andMsgTypeEqualTo(TemplateType);
    	}
        return wxMsgReplyUserMapper.selectByExample(example);
    }
    
    
}
