package org.linlinjava.litemall.db.service;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import me.chanjar.weixin.common.api.WxConsts;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.material.WxMpMaterial;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialArticleUpdate;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialCountResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialFileBatchGetResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialNews;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialUploadResult;
import me.chanjar.weixin.mp.bean.material.WxMpNewsArticle;

@Service
public class WxAssetsService {

	@Autowired
	private WxMpService wxMpService;
	
	/**
	 * 从API获取素材总量
	 * @return
	 * @throws WxErrorException
	 */
    public WxMpMaterialCountResult materialCount() throws WxErrorException{
        return wxMpService.getMaterialService().materialCount();
    }
    
    /**
     * 从API获取图文素材详情
     * @param mediaId
     * @return
     * @throws WxErrorException
     */
    public WxMpMaterialNews materialNewsInfo(String mediaId) throws WxErrorException {
        return wxMpService.getMaterialService().materialNewsInfo(mediaId);
    }
    
    /**
     * 从API获取媒体素材列表
     * @param type
     * @param page
     * @return
     * @throws WxErrorException
     */
    public WxMpMaterialFileBatchGetResult materialFileBatchGet(String type, int page) throws WxErrorException {
        return wxMpService.getMaterialService().materialFileBatchGet(type,0, 100);
    }
    
    /**
     * 上传图文素材
     * @param appid
     * @param articles
     * @return
     * @throws WxErrorException
     */
    public WxMpMaterialUploadResult materialNewsUpload(List<WxMpNewsArticle> articles) throws WxErrorException {
        WxMpMaterialNews news = new WxMpMaterialNews();
        news.setArticles(articles);
        return wxMpService.getMaterialService().materialNewsUpload(news);
    }

    /**
     * 更新图文素材中的某篇文章
     * @param appid
     * @param form
     */
    public void materialArticleUpdate(String appid, WxMpMaterialArticleUpdate form)  throws WxErrorException{
        wxMpService.getMaterialService().materialNewsUpdate(form);
    }
    
    /**
     * 上传媒体素材
     * @param mediaType
     * @param fileName
     * @param file
     * @return
     * @throws WxErrorException
     * @throws IOException
     */
    public WxMpMaterialUploadResult materialFileUpload(String mediaType, String fileName, MultipartFile file) throws WxErrorException, IOException {
        String originalFilename=file.getOriginalFilename();
        File tempFile = File.createTempFile(fileName+"--", Objects.requireNonNull(originalFilename).substring(originalFilename.lastIndexOf(".")));
        file.transferTo(tempFile);
        WxMpMaterial wxMaterial = new WxMpMaterial();
        wxMaterial.setFile(tempFile);
        wxMaterial.setName(fileName);
        if(WxConsts.MediaFileType.VIDEO.equals(mediaType)){
            wxMaterial.setVideoTitle(fileName);
        }
        WxMpMaterialUploadResult res = wxMpService.getMaterialService().materialFileUpload(mediaType,wxMaterial);
        tempFile.deleteOnExit();
        return null;
    }

    /**
     * 删除素材
     * @param mediaId
     * @return
     * @throws WxErrorException
     */
    public boolean materialDelete(String mediaId) throws WxErrorException {
        return wxMpService.getMaterialService().materialDelete(mediaId);
    }
}
