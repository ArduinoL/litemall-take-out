package org.linlinjava.litemall.db.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.linlinjava.litemall.db.domain.LitemallUserBalanceLog;

public interface LitemallUserBalanceLogMapper {

	int insertSelective(LitemallUserBalanceLog record);
	
	List<LitemallUserBalanceLog> selectByShopIdList(@Param("shopId")Integer shopId,@Param("showType")Integer showType);
	
	List<LitemallUserBalanceLog> queryByAddTime(LocalDateTime expired);

	int updateUserBalanceLog(LitemallUserBalanceLog userBalanceLog);
}