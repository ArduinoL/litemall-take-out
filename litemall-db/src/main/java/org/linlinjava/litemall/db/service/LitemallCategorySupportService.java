package org.linlinjava.litemall.db.service;

import com.github.pagehelper.PageHelper;
import org.linlinjava.litemall.db.dao.LitemallCategoryMapper;
import org.linlinjava.litemall.db.dao.LitemallCategorySupportMapper;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.domain.LitemallCategoryExample;
import org.linlinjava.litemall.db.domain.LitemallCategorySupport;
import org.linlinjava.litemall.db.domain.LitemallCategorySupportExample;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class LitemallCategorySupportService {
    @Resource
    private LitemallCategoryMapper categoryMapper;
    @Resource
    private LitemallCategorySupportMapper categorySupportMapper;
    
    
    public List<LitemallCategorySupport> query(int offset, int limit) {
        LitemallCategorySupportExample example = new LitemallCategorySupportExample();
        example.or().andDeletedEqualTo(false);
        example.orderBy("sort_order DESC");
        PageHelper.startPage(offset, limit);
        return categorySupportMapper.selectByExample(example);
    }

    public List<LitemallCategorySupport> queryL0(int offset, int limit) {
        LitemallCategorySupportExample example = new LitemallCategorySupportExample();
        example.or().andLevelEqualTo(0).andDeletedEqualTo(false);
        example.orderBy("sort_order DESC");
        PageHelper.startPage(offset, limit);
        return categorySupportMapper.selectByExample(example);
    }

    public List<LitemallCategorySupport> queryL1(int offset, int limit) {
        LitemallCategorySupportExample example = new LitemallCategorySupportExample();
        example.or().andLevelEqualTo(1).andDeletedEqualTo(false);
        example.orderBy("sort_order DESC");
        PageHelper.startPage(offset, limit);
        return categorySupportMapper.selectByExample(example);
    }
    
    public LitemallCategorySupport findById(Integer id) {
        return categorySupportMapper.selectByPrimaryKey(id);
    }
    
    public void add(LitemallCategorySupport category) {
        category.setAddTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        categorySupportMapper.insertSelective(category);
    }
    
    public int updateById(LitemallCategorySupport category) {
        category.setUpdateTime(LocalDateTime.now());
        return categorySupportMapper.updateByPrimaryKeySelective(category);
    }
    
    public void deleteById(Integer id) {
    	categorySupportMapper.logicalDeleteByPrimaryKey(id);
    }
    /*
    public List<LitemallCategory> queryL1() {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andLevelEqualTo("L1").andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }
    
    public List<LitemallCategory> queryL2(Integer shopId) {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andLevelEqualTo("L2").andDeletedEqualTo(false).andShopIdEqualTo(shopId);
        example.orderBy("sort_order DESC");
        return categoryMapper.selectByExample(example);
    }
    
    public List<LitemallCategory> queryMerchantCategory(LitemallAdmin admin) {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andShopIdEqualTo(admin.getShopId()).andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }

    public List<LitemallCategory> queryByPid(Integer pid) {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andPidEqualTo(pid).andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }
    
    public List<LitemallCategory> queryByShopId(Integer ShopId) {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andShopIdEqualTo(ShopId).andDeletedEqualTo(false);
        example.setOrderByClause("sort_order DESC");
        return categoryMapper.selectByExample(example);
    }
    
    public List<LitemallCategory> queryL2ByIds(List<Integer> ids) {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andIdIn(ids).andLevelEqualTo("L2").andDeletedEqualTo(false);
        return categoryMapper.selectByExample(example);
    }

    public LitemallCategory findById(Integer id) {
        return categoryMapper.selectByPrimaryKey(id);
    }

    public List<LitemallCategory> querySelective(String id, String name, Integer page, Integer size, String sort, String order) {
        LitemallCategoryExample example = new LitemallCategoryExample();
        LitemallCategoryExample.Criteria criteria = example.createCriteria();

        if (!StringUtils.isEmpty(id)) {
            criteria.andIdEqualTo(Integer.valueOf(id));
        }
        if (!StringUtils.isEmpty(name)) {
            criteria.andNameLike("%" + name + "%");
        }
        criteria.andDeletedEqualTo(false);

        if (!StringUtils.isEmpty(sort) && !StringUtils.isEmpty(order)) {
            example.setOrderByClause(sort + " " + order);
        }

        PageHelper.startPage(page, size);
        return categoryMapper.selectByExample(example);
    }

    public int updateById(LitemallCategory category) {
        category.setUpdateTime(LocalDateTime.now());
        return categoryMapper.updateByPrimaryKeySelective(category);
    }

    public void deleteById(Integer id) {
        categoryMapper.logicalDeleteByPrimaryKey(id);
    }

    public void add(LitemallCategory category) {
        category.setAddTime(LocalDateTime.now());
        category.setUpdateTime(LocalDateTime.now());
        categoryMapper.insertSelective(category);
    }

    public List<LitemallCategory> queryChannel() {
        LitemallCategoryExample example = new LitemallCategoryExample();
        example.or().andLevelEqualTo("L1").andDeletedEqualTo(false);
        return categoryMapper.selectByExampleSelective(example, CHANNEL);
    }*/
}
