package org.linlinjava.litemall.db.dao;


import org.linlinjava.litemall.db.domain.LitemallAgentGoods;

public interface LitemallAgentGoodsMapper {
    
    LitemallAgentGoods getAgentGoodsIds(Integer userId);
    
    int insertAgentGoods(LitemallAgentGoods agentGoods);
    
    int updateById(LitemallAgentGoods agentGoods);
}