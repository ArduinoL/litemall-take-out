package org.linlinjava.litemall.db.util;

public class AppPushConstant {

    /**
     * 推送状态：0-未推送；1-推送成功；2-推送失败；3-服务器响应异常
     */
	public static final byte NOT_PUSH = 0;
	public static final byte SUCCESS = 1;
	public static final byte ERROR_PUSH = 2;
	public static final byte ERROR_SERVER = 3;
}
