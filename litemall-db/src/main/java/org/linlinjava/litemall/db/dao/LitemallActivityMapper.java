package org.linlinjava.litemall.db.dao;



import org.linlinjava.litemall.db.domain.LitemallActivity;

public interface LitemallActivityMapper {
    
	LitemallActivity getActivityByUserId(Integer userId);
	
	LitemallActivity getActivityById(Integer id);
    
    int insertActivity(LitemallActivity ay);
    
    int updateById(LitemallActivity ay);
    
    long getActivityByUserIdCount(Integer userId);
    
    long getGoodsA(Integer userId);
    
    LitemallActivity getOneGift(Integer userId);
}