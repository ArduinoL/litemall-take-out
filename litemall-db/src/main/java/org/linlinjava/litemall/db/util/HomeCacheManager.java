package org.linlinjava.litemall.db.util;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 简单缓存的数据
 */
public class HomeCacheManager {
    public static final boolean ENABLE = false;
    public static final String INDEX = "index";
    public static final String CATALOG = "catalog";
    public static final String GOODS = "goods";
    public static final String GOODSLIST = "goodsList";
    public static final String TOKEN = "token";

    private static ConcurrentHashMap<String, Map<String, Object>> cacheDataList = new ConcurrentHashMap<>();

    /**
     * 缓存首页数据
     *
     * @param data
     */
    public static void loadData(String cacheKey, Map<String, Object> data) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        //有记录，则先丢弃
        if (cacheData != null) {
            cacheData.remove(cacheKey);
        }

        cacheData = new HashMap<>();
        //深拷贝
        cacheData.putAll(data);
        cacheData.put("isCache", "true");
        //设置缓存有效期为30分钟
        cacheData.put("expireTime", LocalDateTime.now().plusMinutes(30));
        cacheDataList.put(cacheKey, cacheData);
    }
    
    /**
     * 缓存全部商品数据，直到后台更新商品信息
     * @param cacheKey
     * @param data
     */
    public static void loadGoodsData(String cacheKey, Map<String, Object> data) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        //有记录，则先丢弃
        if (cacheData != null) {
            cacheData.remove(cacheKey);
        }

        cacheData = new HashMap<>();
        //深拷贝
        cacheData.putAll(data);
        cacheData.put("isCache", "true");
        //设置缓存有效期为10分钟
        //cacheData.put("expireTime", LocalDateTime.now().plusMinutes(10));
        cacheDataList.put(cacheKey, cacheData);
    }
	public static boolean hasGoodsData(String cacheKey) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        if (cacheData == null) {
            return false;
        }
        return true;
	}

    public static void loadTokenData(String cacheKey, Map<String, Object> data) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        //有记录，则先丢弃
        if (cacheData != null) {
            cacheData.remove(cacheKey);
        }

        cacheData = new HashMap<>();
        //深拷贝
        cacheData.putAll(data);
        cacheData.put("isCache", "true");
        //设置缓存有效期为120分钟(2小时)
        cacheData.put("expireTime", LocalDateTime.now().plusMinutes(120));
        cacheDataList.put(cacheKey, cacheData);
    }
    
    public static void loadShopNumberData(String cacheKey, Map<String, Object> data) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        //有记录，则先丢弃
        if (cacheData != null) {
            cacheData.remove(cacheKey);
        }

        cacheData = new HashMap<>();
        //深拷贝
        cacheData.putAll(data);
        cacheData.put("isCache", "true");
        //设置缓存有效期为720分钟(12小时)
        cacheData.put("expireTime", LocalDateTime.now().plusMinutes(720));
        cacheDataList.put(cacheKey, cacheData);
    }
    
    public static Map<String, Object> getCacheData(String cacheKey) {
        return cacheDataList.get(cacheKey);
    }

    /**
     * 判断缓存中是否有数据
     *
     * @return
     */
    public static boolean hasData(String cacheKey) {
    	/*
        if (!ENABLE)
            return false;*/

        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        if (cacheData == null) {
            return false;
        } else {
            LocalDateTime expire = (LocalDateTime) cacheData.get("expireTime");
            if (expire.isBefore(LocalDateTime.now())) {
                return false;
            } else {
                return true;
            }
        }
    }
    
    public static boolean hasData2(String cacheKey) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        if (cacheData == null) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * 清除所有缓存
     */
    public static void clearAll() {
        cacheDataList = new ConcurrentHashMap<>();
    }

    /**
     * 清除缓存数据
     */
    public static void clear(String cacheKey) {
        Map<String, Object> cacheData = cacheDataList.get(cacheKey);
        if (cacheData != null) {
            cacheDataList.remove(cacheKey);
        }
    }


}
