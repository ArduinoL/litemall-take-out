package org.linlinjava.litemall.db.dao;


import org.linlinjava.litemall.db.domain.LitemallUserSign;

public interface LitemallUserSignMapper {
    

	LitemallUserSign getOneSignUser(Integer userId);
	
	int updateByUserId(LitemallUserSign us);
	
    int insertUserSign(LitemallUserSign us);
    
}