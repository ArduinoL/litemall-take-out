package org.linlinjava.litemall.db.service;

import java.util.List;

import org.linlinjava.litemall.db.dao.LitemallShopRateMapper;
import org.linlinjava.litemall.db.domain.LitemallShopRate;
import org.linlinjava.litemall.db.domain.LitemallShopRateExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LitemallShopRateService {

	@Autowired
	private LitemallShopRateMapper shopRateMapper;
	
	
	public List<LitemallShopRate> query(){
		LitemallShopRateExample example = new LitemallShopRateExample();
		return shopRateMapper.selectByExample(example);
	}
	
	public LitemallShopRate getShopRateById(Integer id) {
		return shopRateMapper.selectByPrimaryKey(id);
	}
	
	public int updataById(LitemallShopRate record) {
		return shopRateMapper.updateByPrimaryKeySelective(record);
	}
	
	public int add(LitemallShopRate record) {
		return shopRateMapper.insert(record);
	}
}
