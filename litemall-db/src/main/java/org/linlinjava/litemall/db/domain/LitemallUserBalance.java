package org.linlinjava.litemall.db.domain;

import java.math.BigDecimal;
import java.time.LocalDateTime;


public class LitemallUserBalance {

	private Integer id;
	
	private Integer userId;
	
	private Integer shopId;
	
	private Double yield;
	
	private Integer type;
	
	private BigDecimal balance;
	
	private BigDecimal notCashing;
	
	private BigDecimal freeze;
	
	private Integer cashingNum;
	
	private LocalDateTime addTime;
	
	private LocalDateTime updateTime;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getShopId() {
		return shopId;
	}

	public void setShopId(Integer shopId) {
		this.shopId = shopId;
	}

	public Double getYield() {
		return yield;
	}

	public void setYield(Double yield) {
		this.yield = yield;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getNotCashing() {
		return notCashing;
	}

	public void setNotCashing(BigDecimal notCashing) {
		this.notCashing = notCashing;
	}

	public BigDecimal getFreeze() {
		return freeze;
	}

	public void setFreeze(BigDecimal freeze) {
		this.freeze = freeze;
	}

	public Integer getCashingNum() {
		return cashingNum;
	}

	public void setCashingNum(Integer cashingNum) {
		this.cashingNum = cashingNum;
	}

	public LocalDateTime getAddTime() {
		return addTime;
	}

	public void setAddTime(LocalDateTime addTime) {
		this.addTime = addTime;
	}

	public LocalDateTime getUpdateTime() {
		return updateTime;
	}

	public void setUpdateTime(LocalDateTime updateTime) {
		this.updateTime = updateTime;
	}

	@Override
	public String toString() {
		return "LitemallUserBalance [id=" + id + ", userId=" + userId + ", shopId=" + shopId + ", yield=" + yield
				+ ", type=" + type + ", balance=" + balance + ", notCashing=" + notCashing + ", freeze=" + freeze
				+ ", cashingNum=" + cashingNum + ", addTime=" + addTime + ", updateTime=" + updateTime + "]";
	}

	
	
}
