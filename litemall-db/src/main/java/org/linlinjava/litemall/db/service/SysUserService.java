package org.linlinjava.litemall.db.service;

import java.util.List;

import org.linlinjava.litemall.db.dao.SysUserMapper;
import org.linlinjava.litemall.db.domain.SysUser;
import org.linlinjava.litemall.db.domain.SysUserExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageHelper;

@Service
public class SysUserService {
	
	@Autowired
	private SysUserMapper sysUserMapper;

	public List<SysUser> querySelective(String userName, String phone, Integer page, Integer limit){
		SysUserExample example = new SysUserExample();
		SysUserExample.Criteria criteria =  example.createCriteria();
		
		/*
		if(!StringUtils.isEmpty(userName)) {
			criteria.andUserNameLike("%" + userName + "%");
		}
		if(!StringUtils.isEmpty(phone)) {
			criteria.andPhonenumberLike("%" + phone + "%");
		}*/
		
		PageHelper.startPage(page, limit);
		return sysUserMapper.selectByMerchant(example);
	}
	
	public SysUser getOneById(Long userId) {
		return sysUserMapper.selectByPrimaryKey(userId);
	}
}
