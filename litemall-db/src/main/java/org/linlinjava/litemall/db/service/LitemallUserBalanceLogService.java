package org.linlinjava.litemall.db.service;

import org.linlinjava.litemall.db.dao.LitemallUserBalanceLogMapper;
import org.linlinjava.litemall.db.domain.LitemallUserBalanceLog;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

import javax.annotation.Resource;

@Service
public class LitemallUserBalanceLogService {
    @Resource
    private LitemallUserBalanceLogMapper userBalanceLogMapper;




    public void add(LitemallUserBalanceLog userbalanceLog) {
        userBalanceLogMapper.insertSelective(userbalanceLog);
    }

    public List<LitemallUserBalanceLog> selectByShopIdList(Integer shopId,Integer showType){
    	if(showType == 1) {
    		showType = 0;
    	}else {
    		showType = null;
    	}
    	return userBalanceLogMapper.selectByShopIdList(shopId,showType);
    }
    
    public List<LitemallUserBalanceLog> queryByAddTime(int day){
        LocalDateTime now = LocalDateTime.now();
        LocalDateTime expired = now.minusDays(day);
    	return userBalanceLogMapper.queryByAddTime(expired);
    }
    
    public int updateUserBalanceLog(LitemallUserBalanceLog userBalanceLog) {
    	return userBalanceLogMapper.updateUserBalanceLog(userBalanceLog);
    }
}
