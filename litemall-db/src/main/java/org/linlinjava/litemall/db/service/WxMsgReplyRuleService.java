package org.linlinjava.litemall.db.service;

import java.time.LocalDateTime;
import java.util.List;

import org.linlinjava.litemall.db.dao.WxMsgReplyRuleMapper;
import org.linlinjava.litemall.db.domain.WxMsgReplyRule;
import org.linlinjava.litemall.db.domain.WxMsgReplyRuleExample;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageHelper;

@Service
public class WxMsgReplyRuleService {

	@Autowired
	private WxMsgReplyRuleMapper wxMsgReplyRuleMapper;
	
	public List<WxMsgReplyRule> selectByExample(String ruleName,Integer page,Integer limit){
		WxMsgReplyRuleExample example = new WxMsgReplyRuleExample();
		WxMsgReplyRuleExample.Criteria criteria = example.createCriteria();
		
		if(!StringUtils.isEmpty(ruleName)) {
			criteria.andRuleNameLike("%" + ruleName + "%");
		}
		PageHelper.startPage(page, limit);
		return wxMsgReplyRuleMapper.selectByExample(example);
	}
	
	public int insertSelective(WxMsgReplyRule record) {
		record.setUpdateTime(LocalDateTime.now());
		return wxMsgReplyRuleMapper.insertSelective(record);
	}
	
	public int delMsg(Integer ruleId) {
		return wxMsgReplyRuleMapper.deleteByPrimaryKey(ruleId);
	}
	
	public int editMsg(WxMsgReplyRule record) {
		record.setUpdateTime(LocalDateTime.now());
		return wxMsgReplyRuleMapper.updateByPrimaryKey(record);
	}
	
	public List<WxMsgReplyRule> selectAll(String keywords){
		WxMsgReplyRuleExample example = new WxMsgReplyRuleExample();
		WxMsgReplyRuleExample.Criteria criteria = example.createCriteria();
		if(!StringUtils.isEmpty(keywords)) {
			criteria.andMatchValueLike("%" + keywords + "%");
		}
		return wxMsgReplyRuleMapper.selectAll(keywords);
	}
}
