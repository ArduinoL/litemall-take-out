package org.linlinjava.litemall.db.dao;



import java.util.List;
import java.util.Map;

import org.linlinjava.litemall.db.domain.LitemallActiveUser;

public interface LitemallActiveUserMapper {
    

	List<Map> getActiveUser();
	
    int insertActiveUser(LitemallActiveUser ar);
    
}