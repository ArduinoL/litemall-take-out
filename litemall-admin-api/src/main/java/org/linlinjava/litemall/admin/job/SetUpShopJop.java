package org.linlinjava.litemall.admin.job;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallSchool;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceLog;
import org.linlinjava.litemall.db.service.LitemallSchoolService;
import org.linlinjava.litemall.db.service.LitemallShopBalanceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 门店收入统计
 * @author Administrator
 *
 */
@Component
public class SetUpShopJop {

	private final Log logger = LogFactory.getLog(SetUpShopJop.class);
	
	@Autowired
	private LitemallSchoolService schoolService;
	
    /*
     * 定时凌晨8点开启上楼服务 	 0 0 8 * * ? 
     */
	//@Scheduled(cron = "0 0 8 * * ?")
	public void openOff() {
		logger.info("系统开启定时任务--学校上楼开关--开始");
		
		LitemallSchool school = new LitemallSchool();
		school.setId(1);
		school.setPayOff(0);//开启上楼服务
		schoolService.updateById(school);
		
		logger.info("系统关闭定时任务--学校上楼开关--结束");
	}
	
    /*
     * 定时15点开启上楼服务 	 0 0 15 * * ? 
     */
	//@Scheduled(cron = "0 0 15 * * ?")
	public void openOff2() {
		logger.info("系统开启定时任务--学校上楼开关--开始");
		
		LitemallSchool school = new LitemallSchool();
		school.setId(1);
		school.setPayOff(0);//开启上楼服务
		schoolService.updateById(school);
		
		logger.info("系统关闭定时任务--学校上楼开关--结束");
	}
	
    /*
     * 定时12点45关闭上楼服务 
     */
	//@Scheduled(cron = "0 45 12 * * ?")
	public void closeOff() {
		logger.info("系统开启定时任务--学校上楼关闭--开始");
		
		LitemallSchool school = new LitemallSchool();
		school.setId(1);
		school.setPayOff(1);//关闭上楼服务
		schoolService.updateById(school);
		
		logger.info("系统关闭定时任务--学校上楼关闭--结束");
	}
	
    /*
     * 定时18点45关闭上楼服务 
     */
	//@Scheduled(cron = "0 45 18 * * ?")
	public void closeOff2() {
		logger.info("系统开启定时任务--学校上楼关闭--开始");
		
		LitemallSchool school = new LitemallSchool();
		school.setId(1);
		school.setPayOff(1);//关闭上楼服务
		schoolService.updateById(school);
		
		logger.info("系统关闭定时任务--学校上楼关闭--结束");
	}
}
