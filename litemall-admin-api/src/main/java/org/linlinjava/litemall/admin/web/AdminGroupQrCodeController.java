package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallGroupQrcode;
import org.linlinjava.litemall.db.service.LitemallGroupQrCodeService;
import org.linlinjava.litemall.db.util.HomeCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/admin/groupQrcode")
@Validated
public class AdminGroupQrCodeController {
    private final Log logger = LogFactory.getLog(AdminGroupQrCodeController.class);

    @Autowired
    private LitemallGroupQrCodeService GroupQrCodeService;

    @RequiresPermissions("admin:groupQrcode:list")
    @RequiresPermissionsDesc(menu = {"群二维码管理", "群二维码管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<LitemallGroupQrcode> qrList = GroupQrCodeService.querySelective(page, limit);
        return ResponseUtil.okList(qrList);
    }

    private Object validate(LitemallGroupQrcode groupQrcode) {
        String title = groupQrcode.getTitle();
        if (StringUtils.isEmpty(title)) {
            return ResponseUtil.badArgument();
        }
        String qrUrl = groupQrcode.getQrUrl();
        if (StringUtils.isEmpty(qrUrl)) {
            return ResponseUtil.badArgument();
        }
        Integer ScanNum = groupQrcode.getScanNum();
        if (ScanNum == null) {
            return ResponseUtil.badArgument();
        }
        Integer MaxNum = groupQrcode.getMaxNum();
        if (MaxNum == null) {
            return ResponseUtil.badArgument();
        }
        return null;
    }

    @RequiresPermissions("admin:groupQrcode:create")
    @RequiresPermissionsDesc(menu = {"群二维码管理", "群二维码管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody LitemallGroupQrcode groupQrcode) {
        Object error = validate(groupQrcode);
        if (error != null) {
            return error;
        }
        //外链地址  xxx?uuid=uuid
        String uuid = UUID.randomUUID().toString().replaceAll("-", "");
        groupQrcode.setUuid(uuid);
        groupQrcode.setUrl("http://qr.91tanchi.com?uuid="+uuid);
        GroupQrCodeService.add(groupQrcode);
        return ResponseUtil.ok(groupQrcode);
    }

    @RequiresPermissions("admin:groupQrcode:read")
    @RequiresPermissionsDesc(menu = {"群二维码管理", "群二维码管理"}, button = "详情")
    @GetMapping("/read")
    public Object read(@NotNull Integer id) {
    	LitemallGroupQrcode groupQrcode = GroupQrCodeService.findById(id);
        return ResponseUtil.ok(groupQrcode);
    }

    @RequiresPermissions("admin:groupQrcode:update")
    @RequiresPermissionsDesc(menu = {"群二维码管理", "群二维码管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallGroupQrcode groupQrcode) {
        Object error = validate(groupQrcode);
        if (error != null) {
            return error;
        }
        if (GroupQrCodeService.updateById(groupQrcode) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        //清除缓存
        HomeCacheManager.clear(groupQrcode.getUuid().toString());
        return ResponseUtil.ok(groupQrcode);
    }

    @RequiresPermissions("admin:groupQrcode:delete")
    @RequiresPermissionsDesc(menu = {"群二维码管理", "群二维码管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallGroupQrcode groupQrcode) {
        Integer id = groupQrcode.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        GroupQrCodeService.deleteById(id);
        return ResponseUtil.ok();
    }

}
