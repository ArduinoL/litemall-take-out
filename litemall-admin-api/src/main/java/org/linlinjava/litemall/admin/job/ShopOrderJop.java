package org.linlinjava.litemall.admin.job;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceLog;
import org.linlinjava.litemall.db.service.LitemallShopBalanceLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 门店收入统计
 * @author Administrator
 *
 */
@Component
public class ShopOrderJop {

	private final Log logger = LogFactory.getLog(ShopOrderJop.class);
	
	@Autowired
	private LitemallShopBalanceLogService shopBalanceLogService;
	
    /*
     * 定时时间是每天凌晨5点。 0 0 5 * * ? 
     */
	@Scheduled(cron = "0 0 5 * * ?")
	//@Scheduled(cron = "0 0 14 * * ?")//目前改成14点统计
	public void Orderfinal1() {
		logger.info("系统开启定时任务--商家收入结算--开始");
		//int day = 1;//结算周期 1个工作日
		List<LitemallShopBalanceLog> shopBalanceLog = shopBalanceLogService.querySelective();
		
		for(LitemallShopBalanceLog shopBLog : shopBalanceLog) {

			if(shopBalanceLogService.updateShopBalanceLogANDBalance(shopBLog)==0) {
				logger.info("结算单ID=" + shopBLog.getId() + "结算不成功!!!!!!!!!!!!");
			}else {
				//logger.info("结算单ID=" + shopBLog.getId() + "结算成功");
			}
		}
		logger.info("系统关闭定时任务--商家收入结算--结束");
	}
}
