package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.domain.LitemallSchool;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.service.LitemallSchoolService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/school")
@Validated
public class AdminSchoolController {
    private final Log logger = LogFactory.getLog(AdminSchoolController.class);

    @Autowired
    private LitemallSchoolService schoolService;
    @Autowired
    private LitemallShopService shopService;

    @RequiresPermissions("admin:school:list")
    @RequiresPermissionsDesc(menu = {"学校管理", "学校管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String schoolName,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
        List<LitemallSchool> ShopList = schoolService.querySelective(schoolName, page, limit);
        return ResponseUtil.okList(ShopList);
    }

    /*
    private Object validate(LitemallSchool school) {
        String SchoolName = school.getSchoolName();
        if (StringUtils.isEmpty(SchoolName)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }*/

    @RequiresPermissions("admin:school:create")
    @RequiresPermissionsDesc(menu = {"学校管理", "学校管理"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody LitemallSchool school) {
    	schoolService.add(school);
        return ResponseUtil.ok(school);
    }


    @RequiresPermissions("admin:school:update")
    @RequiresPermissionsDesc(menu = {"学校管理", "学校管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallSchool school) {
        if (schoolService.updateById(school) == 0) {
            return ResponseUtil.updatedDataFailed();
        }

        return ResponseUtil.ok(school);
    }
    
    @RequiresPermissions("admin:school:updateOff")
    @RequiresPermissionsDesc(menu = {"学校管理", "学校管理"}, button = "编辑")
    @PostMapping("/updateOff")
    public Object updateOff(@RequestBody LitemallSchool school) {
    	LitemallSchool s = new LitemallSchool();
    	s.setId(school.getId());
    	s.setPayOff(school.getPayOff());
        if (schoolService.updateById(s) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok(school);
    }
    
    @PostMapping("/selectShopIds")
    public Object selectShopIds(@RequestBody LitemallSchool school) {
    	/*
    	//LitemallSchool school = new LitemallSchool();
    	String ShopIds = "";
    	String shopIds = JacksonUtil.parseString(ShopIds, "ShopIds");
    	String newIds = shopIds.substring(1, shopIds.length()-1);
    	String ShopIdss[] = newIds.split(",");
    	Map<String, Object> entity = new HashMap<>();*/
    	Integer[] shopIds = new Integer[0];
    	if(school == null) {
    		return ResponseUtil.ok(shopIds);
    	}
    	if(StringUtils.isEmpty(school.getShopIds())) {
    		return ResponseUtil.ok(shopIds);
    	}
    	if(school.getShopIds().length<1) {
    		return ResponseUtil.ok(shopIds);
    	}
    	/*
    	shopIds = shopService.selectShopIds(school.getShopIds());
    	entity.put("shopIds", school.getShopIds());*/
    	List<LitemallShop> llistShop = shopService.selectlistShop(school.getShopIds());
    	List<Map<String, Object>> data = new ArrayList<>(llistShop.size());
        for (LitemallShop s : llistShop) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("key", s.getId());
            d.put("label", s.getShopName());
            data.add(d);
        }
    	Integer[] array = new Integer[llistShop.size()];
        for(int i=0;i<llistShop.size();i++) {
        	array[i] = llistShop.get(i).getId();
        }
        
    	return ResponseUtil.ok(array);
    }
/*
    @RequiresPermissions("admin:school:delete")
    @RequiresPermissionsDesc(menu = {"学校管理", "学校管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallSchool shop) {
        Integer id = shop.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        //schoolService.delShop(id);
        return ResponseUtil.ok();
    }
    */
    @GetMapping("/listAdmin")
    public Object listAdmin() {
    	List<LitemallAdmin> listAdmin = schoolService.getlistAdmin();
        return ResponseUtil.okList(listAdmin);
    }
    
    @GetMapping("/selectShop")
    public Object selectShop() {
    	//全部店铺（包括新加入店铺还未设置过基本信息的）
    	List<LitemallShop> listShop = shopService.selectShop();
    	List<Map<String, Object>> data = new ArrayList<>(listShop.size());
        for (LitemallShop s : listShop) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("key", s.getId());
            d.put("label", s.getShopName());
            data.add(d);
        }
    	return ResponseUtil.okList(data);
    }

}
