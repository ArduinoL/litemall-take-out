package org.linlinjava.litemall.admin.web.merchant;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.admin.dto.GoodsAllinone;
import org.linlinjava.litemall.admin.service.AdminGoodsService;
import org.linlinjava.litemall.core.storage.StorageService;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.validator.Order;
import org.linlinjava.litemall.core.validator.Sort;
import org.linlinjava.litemall.db.domain.LitemallAdmin;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.domain.LitemallStorage;
import org.linlinjava.litemall.db.util.HomeCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

import javax.validation.constraints.NotNull;

@RestController
@RequestMapping("/admin/merchant_goods")
@Validated
public class MerchantGoodsController {
    private final Log logger = LogFactory.getLog(MerchantGoodsController.class);

    @Autowired
    private AdminGoodsService adminGoodsService;
    @Autowired
    private StorageService storageService;

    /**
     * 查询商品
     *
     * @param goodsId
     * @param goodsSn
     * @param name
     * @param page
     * @param limit
     * @param sort
     * @param order
     * @return
     */
    @RequiresPermissions("admin:merchant_goods:list")
    @RequiresPermissionsDesc(menu = {"商家商品管理", "商品管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(Integer goodsId, String goodsSn, String name,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
    	LitemallAdmin admin = (LitemallAdmin) SecurityUtils.getSubject().getPrincipal();
        return adminGoodsService.merchant_list(admin, goodsId, goodsSn, name, page, limit, sort, order);
    }

    @GetMapping("/catAndBrand")
    public Object list2() {
    	LitemallAdmin admin = (LitemallAdmin) SecurityUtils.getSubject().getPrincipal();
        return adminGoodsService.getMerchantCategoryL2(admin);
    }

    /**
     * 编辑商品
     *
     * @param goodsAllinone
     * @return
     */
    @RequiresPermissions("admin:merchant_goods:update")
    @RequiresPermissionsDesc(menu = {"商家商品管理", "商品管理"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody GoodsAllinone goodsAllinone) {
    	//清除缓存数据
    	HomeCacheManager.clearAll();
        return adminGoodsService.update(goodsAllinone);
    }

    /**
     * 删除商品
     *
     * @param goods
     * @return
     */
    @RequiresPermissions("admin:merchant_goods:delete")
    @RequiresPermissionsDesc(menu = {"商家商品管理", "商品管理"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallGoods goods) {
    	//清除缓存数据
    	HomeCacheManager.clearAll();
        return adminGoodsService.delete(goods);
    }

    /**
     * 添加商品
     *
     * @param goodsAllinone
     * @return
     */
    @RequiresPermissions("admin:merchant_goods:create")
    @RequiresPermissionsDesc(menu = {"商家商品管理", "商品管理"}, button = "上架")
    @PostMapping("/create")
    public Object create(@RequestBody GoodsAllinone goodsAllinone) {
    	//清除缓存数据
    	HomeCacheManager.clearAll();
    	LitemallAdmin admin = (LitemallAdmin) SecurityUtils.getSubject().getPrincipal();
        return adminGoodsService.create(goodsAllinone,admin);
    }

    /**
     * 商品详情
     *
     * @param id
     * @return
     */
    @RequiresPermissions("admin:merchant_goods:read")
    @RequiresPermissionsDesc(menu = {"商家商品管理", "商品管理"}, button = "详情")
    @GetMapping("/detail")
    public Object detail(@NotNull Integer id) {
        return adminGoodsService.detail(id);

    }
    
    @RequiresPermissions("admin:merchant_goods:create_upload")
    @RequiresPermissionsDesc(menu = {"商家商品管理", "对象存储"}, button = "上传")
    @PostMapping("/create_upload")
    public Object create_upload(@RequestParam("file") MultipartFile file) throws IOException {
        String originalFilename = file.getOriginalFilename();
        LitemallStorage litemallStorage = storageService.store(file.getInputStream(), file.getSize(),
                file.getContentType(), originalFilename);
        return ResponseUtil.ok(litemallStorage);
    }

}
