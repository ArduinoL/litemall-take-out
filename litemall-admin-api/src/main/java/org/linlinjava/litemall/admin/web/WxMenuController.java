package org.linlinjava.litemall.admin.web;


import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.bean.menu.WxMenu;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.menu.WxMpMenu;

@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/wxMenu")
@Validated
public class WxMenuController {

	private final Log logger = LogFactory.getLog(WxMenuController.class);
	
	@Autowired
	private WxMpService wxMpService;
	
    @RequiresPermissions("admin:wxMenu:getMenu")
    @RequiresPermissionsDesc(menu = {"公众号管理", "公众号菜单"}, button = "获取菜单")
    @GetMapping("/getMenu")
    public Object getMenu() throws WxErrorException {
    	WxMpMenu wxMpMenu = wxMpService.getMenuService().menuGet();
    	return ResponseUtil.ok(wxMpMenu);
    }
    
    @RequiresPermissions("admin:wxMenu:updateMenu")
    @RequiresPermissionsDesc(menu = {"公众号管理", "公众号菜单"}, button = "创建更新菜单")
    @PostMapping("/updateMenu")
    public Object updateMenu(@RequestBody WxMenu wxMenu) throws WxErrorException {
    	return ResponseUtil.ok(wxMpService.getMenuService().menuCreate(wxMenu));
    }
    
}
