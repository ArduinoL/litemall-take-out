package org.linlinjava.litemall.admin.wx.handler;

import me.chanjar.weixin.common.session.WxSessionManager;
import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.bean.message.WxMpXmlMessage;
import me.chanjar.weixin.mp.bean.message.WxMpXmlOutMessage;

import org.linlinjava.litemall.db.service.WxUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * @author Binary Wang
 */
@Component
public class UnsubscribeHandler extends AbstractHandler {

	@Autowired
	private WxUserService wxUserService;

	@Override
	public WxMpXmlOutMessage handle(WxMpXmlMessage wxMessage, Map<String, Object> context, WxMpService wxMpService,
			WxSessionManager sessionManager) {
		//String openId = wxMessage.getFromUser();
		// this.logger.info("取消关注用户 OPENID: " + openId);
		// TODO 可以更新本地数据库为取消关注状态
		wxUserService.unsubscribe(wxMessage.getFromUser());

		return null;
	}

}
