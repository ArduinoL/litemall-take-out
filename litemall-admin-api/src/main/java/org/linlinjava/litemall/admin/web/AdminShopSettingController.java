package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallShopSetting;
import org.linlinjava.litemall.db.service.LitemallShopSettingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.LocalTime;

@RestController
@RequestMapping("/admin/shopsetting")
@Validated
public class AdminShopSettingController {
    private final Log logger = LogFactory.getLog(AdminShopSettingController.class);
    
    @Autowired
    private LitemallShopSettingService shopSettingService;


    @RequiresPermissions("admin:shopsetting:create")
    @RequiresPermissionsDesc(menu = {"门店设置", "门店设置"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody LitemallShopSetting ShopSetting) {
    	Object error = validate(ShopSetting);
        if (error != null) {
            return error;
        }
        return ResponseUtil.ok(shopSettingService.add(ShopSetting));
    }


    @RequiresPermissions("admin:shopsetting:update")
    @RequiresPermissionsDesc(menu = {"门店设置", "门店设置"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallShopSetting ShopSetting) {
    	Object error = validate(ShopSetting);
        if (error != null) {
            return error;
        }
        if (shopSettingService.update(ShopSetting) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        return ResponseUtil.ok(ShopSetting);
    }
    
    @RequiresPermissions("admin:shopsetting:getOneSetting")
    @RequiresPermissionsDesc(menu = {"门店设置", "门店设置"}, button = "查询设置")
    @GetMapping("getOneSetting")
    public Object getOneSetting(Integer ShopId) {
    	if(ShopId == null) {
    		return ResponseUtil.badArgument();
    	}
    	return ResponseUtil.ok(shopSettingService.getOneSetting(ShopId));
    }
    
    private Object validate(LitemallShopSetting ShopSetting) {
        Integer ShopId = ShopSetting.getShopId();
        if (ShopId == null) {
            return ResponseUtil.badArgument();
        }
        String ShopPhone = ShopSetting.getShopPhone();
        if (StringUtils.isEmpty(ShopPhone)) {
            return ResponseUtil.badArgument();
        }
        Integer rate = ShopSetting.getRate();
        if(rate == null) {
        	return ResponseUtil.badArgument();
        }
        BigDecimal deliveryPrice = ShopSetting.getDeliveryPrice();
        if(deliveryPrice == null) {
        	return ResponseUtil.badArgument();
        }
        BigDecimal InitialPrice = ShopSetting.getInitialPrice();
        if(InitialPrice == null) {
        	return ResponseUtil.badArgument();
        }
        String deliveryStart = ShopSetting.getDeliveryStart();
        if(StringUtils.isEmpty(deliveryStart)) {
        	return ResponseUtil.badArgument();
        }
        String DeliveryEnd = ShopSetting.getDeliveryEnd();
        if(StringUtils.isEmpty(DeliveryEnd)) {
        	return ResponseUtil.badArgument();
        }
        return null;
    }

}
