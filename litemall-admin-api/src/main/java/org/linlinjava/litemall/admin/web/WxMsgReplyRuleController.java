package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.WxMsgReplyRule;
import org.linlinjava.litemall.db.service.WxMsgReplyRuleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/wxMsg")
@Validated
public class WxMsgReplyRuleController {

	private final Log logger = LogFactory.getLog(WxMsgReplyRuleController.class);
	
	@Autowired
	private WxMsgReplyRuleService wxMsgReplyRuleService;
	
    @RequiresPermissions("admin:wxMsg:list")
    @RequiresPermissionsDesc(menu = {"自动回复", "自动回复"}, button = "查询")
    @GetMapping("/list")
    public Object getMenu(String ruleName,
            @RequestParam(defaultValue = "1") Integer page,
            @RequestParam(defaultValue = "10") Integer limit){
    	return ResponseUtil.okList(wxMsgReplyRuleService.selectByExample(ruleName, page, limit));
    }
    
    @RequiresPermissions("admin:wxMsg:update")
    @RequiresPermissionsDesc(menu = {"自动回复", "自动回复"}, button = "更新")
    @PostMapping("/update")
    public Object update(@RequestBody WxMsgReplyRule we){
    	return ResponseUtil.ok(wxMsgReplyRuleService.editMsg(we));
    }
    
    @RequiresPermissions("admin:wxMsg:delMsg")
    @RequiresPermissionsDesc(menu = {"自动回复", "自动回复"}, button = "删除")
    @PostMapping("/delMsg")
    public Object delMsg(@RequestBody WxMsgReplyRule we) {
        Integer id = we.getRuleId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        wxMsgReplyRuleService.delMsg(id);
        return ResponseUtil.ok();
    }
    
    @RequiresPermissions("admin:wxMsg:save")
    @RequiresPermissionsDesc(menu = {"自动回复", "自动回复"}, button = "添加")
    @PostMapping("/save")                                                                           
    public Object save(@RequestBody WxMsgReplyRule we) {
    	Object error = validate(we);                                                   
        if (error != null) {
            return error;
        }
    	wxMsgReplyRuleService.insertSelective(we);
    	return ResponseUtil.ok(we);
    }
    
    private Object validate(WxMsgReplyRule we) {
        String ruleName = we.getRuleName();
        if (StringUtils.isEmpty(ruleName)) {
            return ResponseUtil.badArgument();
        }
        
        String matchValue = we.getMatchValue();
        if (StringUtils.isEmpty(matchValue)) {
            return ResponseUtil.badArgument();
        }

        String replyType = we.getReplyType(); 
        if (StringUtils.isEmpty(replyType)) {
            return ResponseUtil.badArgument();
        }
        return null;
    }
}
