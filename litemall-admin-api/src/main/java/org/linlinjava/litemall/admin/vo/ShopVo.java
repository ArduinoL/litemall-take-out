package org.linlinjava.litemall.admin.vo;

import java.math.BigDecimal;

public class ShopVo {

	private Integer id;
	
	private String avatar;
	
	private String picUrl;
	
	private String name;
	
	private String shopName;
	
	private String address;
	
	private String linkman;
	
	private String phone;
	
	private BigDecimal balance;
	
	private BigDecimal notCashing;
	
	private BigDecimal freeze;
	
	private Integer cashingNum;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getAvatar() {
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getShopName() {
		return shopName;
	}

	public void setShopName(String shopName) {
		this.shopName = shopName;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLinkman() {
		return linkman;
	}

	public void setLinkman(String linkman) {
		this.linkman = linkman;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public BigDecimal getNotCashing() {
		return notCashing;
	}

	public void setNotCashing(BigDecimal notCashing) {
		this.notCashing = notCashing;
	}

	public BigDecimal getFreeze() {
		return freeze;
	}

	public void setFreeze(BigDecimal freeze) {
		this.freeze = freeze;
	}

	public Integer getCashingNum() {
		return cashingNum;
	}

	public void setCashingNum(Integer cashingNum) {
		this.cashingNum = cashingNum;
	}


	
	
	
}
