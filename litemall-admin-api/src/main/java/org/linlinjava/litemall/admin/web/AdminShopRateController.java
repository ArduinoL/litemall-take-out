package org.linlinjava.litemall.admin.web;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallShopRate;
import org.linlinjava.litemall.db.service.LitemallShopRateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/shopRate")
@Validated
public class AdminShopRateController {

	private final Log logger = LogFactory.getLog(AdminShopRateController.class);
	
	@Autowired
	private LitemallShopRateService shopRateService;
	
	public Object list() {
		List<LitemallShopRate> shopRateList = shopRateService.query();
		return null;
	}
}
