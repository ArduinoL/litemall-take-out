package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.admin.vo.CategoryVo;
import org.linlinjava.litemall.core.redis.RedisCache;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallCategory;
import org.linlinjava.litemall.db.domain.LitemallCategorySupport;
import org.linlinjava.litemall.db.domain.LitemallSchool;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.service.LitemallCategoryService;
import org.linlinjava.litemall.db.service.LitemallCategorySupportService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.linlinjava.litemall.db.util.HomeCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/admin/categorysupport")
@Validated
public class AdminCategorySupportController {
    private final Log logger = LogFactory.getLog(AdminCategorySupportController.class);

    @Autowired
    private LitemallCategoryService categoryService;
    @Autowired
    private LitemallCategorySupportService categorySupportService;
    @Autowired
    private LitemallShopService shopService;
    @Autowired
    private RedisCache redisCache;

    @RequiresPermissions("admin:categorySupport:list")
    @RequiresPermissionsDesc(menu = {"商场管理", "首页类目"}, button = "查询")
    @GetMapping("/list")
    public Object list() {
        //List<LitemallCategorySupport> categoryList = categorySupportService.queryL0(0, 100);
        return ResponseUtil.okList(categorySupportService.queryL0(0, 20));
    }
    
    @RequiresPermissions("admin:categorySupport:list")
    @RequiresPermissionsDesc(menu = {"商场管理", "首页类目"}, button = "查询")
    @GetMapping("/list2")
    public Object list2() {
        //List<LitemallCategorySupport> categoryList = categorySupportService.queryL0(0, 100);
        return ResponseUtil.okList(categorySupportService.queryL1(0, 100));
    }

    private Object validate(LitemallCategorySupport category) {
        String name = category.getName();
        if (StringUtils.isEmpty(name)) {
            return ResponseUtil.badArgument();
        }
        String Icon = category.getIcon();
        if (StringUtils.isEmpty(Icon)) {
            return ResponseUtil.badArgument();
        }
        String Color = category.getColor();
        if (StringUtils.isEmpty(Color)) {
            return ResponseUtil.badArgument();
        }

        return null;
    }

    @RequiresPermissions("admin:categorySupport:create")
    @RequiresPermissionsDesc(menu = {"商场管理", "首页类目"}, button = "添加")
    @PostMapping("/create")
    public Object create(@RequestBody LitemallCategorySupport category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        categorySupportService.add(category);
        // 清除缓存
        redisCache.deleteObject();
        return ResponseUtil.ok(category);
    }

    @RequiresPermissions("admin:categorySupport:read")
    @RequiresPermissionsDesc(menu = {"商场管理", "首页类目"}, button = "详情")
    @GetMapping("/read")
    public Object read(@NotNull Integer id) {
        LitemallCategory category = categoryService.findById(id);
        return ResponseUtil.ok(category);
    }

    @RequiresPermissions("admin:categorySupport:update")
    @RequiresPermissionsDesc(menu = {"商场管理", "首页类目"}, button = "编辑")
    @PostMapping("/update")
    public Object update(@RequestBody LitemallCategorySupport category) {
        Object error = validate(category);
        if (error != null) {
            return error;
        }
        if (categorySupportService.updateById(category) == 0) {
            return ResponseUtil.updatedDataFailed();
        }
        // 清除缓存
        redisCache.deleteObject();
        return ResponseUtil.ok();
    }

    @RequiresPermissions("admin:categorySupport:delete")
    @RequiresPermissionsDesc(menu = {"商场管理", "首页类目"}, button = "删除")
    @PostMapping("/delete")
    public Object delete(@RequestBody LitemallCategorySupport category) {
        Integer id = category.getId();
        if (id == null) {
            return ResponseUtil.badArgument();
        }
        categorySupportService.deleteById(id);
        // 清除缓存
        redisCache.deleteObject();
        return ResponseUtil.ok();
    }
    
    
    @PostMapping("/selectShopIds")
    public Object selectShopIds(@RequestBody LitemallCategorySupport category) {
    	Integer[] shopIds = new Integer[0];
    	if(category == null) {
    		return ResponseUtil.ok(shopIds);
    	}
    	if(StringUtils.isEmpty(category.getShopIds())) {
    		return ResponseUtil.ok(shopIds);
    	}
    	if(category.getShopIds().length<1) {
    		return ResponseUtil.ok(shopIds);
    	}
    	List<LitemallShop> llistShop = shopService.selectlistShop(category.getShopIds());
    	List<Map<String, Object>> data = new ArrayList<>(llistShop.size());
        for (LitemallShop s : llistShop) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("key", s.getId());
            d.put("label", s.getShopName());
            data.add(d);
        }
    	Integer[] array = new Integer[llistShop.size()];
        for(int i=0;i<llistShop.size();i++) {
        	array[i] = llistShop.get(i).getId();
        }
        
    	return ResponseUtil.ok(array);
    }
    
    @GetMapping("/selectShop")
    public Object selectShop() {
    	//全部店铺（包括新加入店铺还未设置过基本信息的）
    	List<LitemallShop> listShop = shopService.selectShop();
    	List<Map<String, Object>> data = new ArrayList<>(listShop.size());
        for (LitemallShop s : listShop) {
            Map<String, Object> d = new HashMap<>(2);
            d.put("key", s.getId());
            d.put("label", s.getShopName());
            data.add(d);
        }
    	return ResponseUtil.okList(data);
    }

}
