package org.linlinjava.litemall.admin.web.api;

import org.linlinjava.litemall.admin.service.AdminOrderService;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/api")
@Validated
public class ApiController {

    @Autowired 
    private AdminOrderService adminOrderService;

    /**
     * 订单退款
     *
     * @param body 订单信息，{ orderId：xxx }
     * @return 订单退款操作结果
     */
    @PostMapping("/refund")
    public Object refund(@RequestBody String body) {
    	if(StringUtils.isEmpty(body)) {
    		return ResponseUtil.badArgument();
    	}
		Integer orderId = JacksonUtil.parseInteger(body, "orderId");
		String token = JacksonUtil.parseString(body, "token");
		
    	if(!token.equals("$2a$10$TjPt1hgDG7dAKQ6Z7gYpwOQBPPdLSWqFtaUHDGId.rc7BLD.V.Rz2")) {
    		return ResponseUtil.badArgument();
    	}
        return adminOrderService.refund(orderId);
    }
}
