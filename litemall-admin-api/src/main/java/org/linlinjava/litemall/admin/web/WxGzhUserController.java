package org.linlinjava.litemall.admin.web;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.core.validator.Order;
import org.linlinjava.litemall.core.validator.Sort;
import org.linlinjava.litemall.db.domain.WxUser;
import org.linlinjava.litemall.db.service.WxUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/gzh")
@Validated
public class WxGzhUserController {

	private final Log logger = LogFactory.getLog(WxGzhUserController.class);
	
	@Autowired
	private WxUserService wxUserService;
	
	
    @RequiresPermissions("admin:gzh:getwxUser")
    @RequiresPermissionsDesc(menu = {"公众号管理", "粉丝管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String username, String mobile,
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit,
                       @Sort @RequestParam(defaultValue = "add_time") String sort,
                       @Order @RequestParam(defaultValue = "desc") String order) {
        List<WxUser> userList = wxUserService.querySelective(username,page, limit);
        return ResponseUtil.okList(userList);
    }
    
    @RequiresPermissions("admin:gzh:save")
    @RequiresPermissionsDesc(menu = {"公众号管理", "同步粉丝"}, button = "保存")
    @PostMapping("/syncWxUsers")
    public Object syncWxUsers() {
    	wxUserService.syncWxUsers();
    	return ResponseUtil.ok();
    }
}
