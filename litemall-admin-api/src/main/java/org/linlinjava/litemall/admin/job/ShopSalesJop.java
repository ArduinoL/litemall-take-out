package org.linlinjava.litemall.admin.job;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.redis.RedisCache;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallShopBalanceLog;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.service.LitemallShopBalanceLogService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 门店收入统计
 * @author Administrator
 *
 */
@Component
public class ShopSalesJop {

	private final Log logger = LogFactory.getLog(ShopSalesJop.class);
	
	@Autowired
	private LitemallOrderService orderService;
	@Autowired
	private LitemallShopService shopService;
	@Autowired
	private RedisCache redisCache;
	
    /*
     * 定时时间是每天凌晨0点5分。 0 5 0 * * ?  	
     */
	@Scheduled(cron = "0 5 0 * * ?")
	public void ShopSales() {
		logger.info("系统开启定时任务--商家销量统计--开始");
		
		List<LitemallShop> shopList = shopService.getShopList();
		Integer number = 0;
		for(int i=0;i<shopList.size();i++) {
			//清除商家订单序号
			redisCache.deleteObject("shop_" + shopList.get(i).getId());
			List<LitemallOrder> orderList = orderService.selectByYesterdayOrder(shopList.get(i).getId());
			if(orderList.size()>0) {
				number = orderList.size() + 22;
				//修改商家月售
				LitemallShop shop = shopService.getOneByShopID(shopList.get(i).getId());
				//LitemallShop shop = new LitemallShop();
				shop.setSell(shop.getSell()+number);
				if(shopService.updateById(shop)==0) {
					logger.info("系统开启定时任务--商家销量统计失败--shopId="+shop.getId());
				}
			}

		}
		
		logger.info("系统关闭定时任务--商家销量统计--结束");
	}
}
