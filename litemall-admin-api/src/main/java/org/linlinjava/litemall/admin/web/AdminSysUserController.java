package org.linlinjava.litemall.admin.web;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.service.SysUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/admin/sysuser")
@Validated
public class AdminSysUserController {
	
	@Autowired
	private SysUserService sysUserService;

    @RequiresPermissions("admin:sysuser:list")
    @RequiresPermissionsDesc(menu = {"app用户管理", "用户管理"}, button = "查询")
    @GetMapping("/list")
    public Object list(String userName, String phone, 
                       @RequestParam(defaultValue = "1") Integer page,
                       @RequestParam(defaultValue = "10") Integer limit) {
    	
        return ResponseUtil.okList(sysUserService.querySelective(userName, phone, page, limit));
    }
}
