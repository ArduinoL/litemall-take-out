package org.linlinjava.litemall.admin.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.linlinjava.litemall.admin.annotation.RequiresPermissionsDesc;
import org.linlinjava.litemall.core.template.WxMpMsgTemplateService;
import org.linlinjava.litemall.core.util.JacksonUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/push")
@Validated
public class PushMsgController {

	private final Log logger = LogFactory.getLog(PushMsgController.class);
	
	@Autowired
	private WxMpMsgTemplateService wxMpMsgTemplateService;
	
    
    @RequiresPermissions("admin:push:pushGZHmsg")
    @RequiresPermissionsDesc(menu = {"推送消息", "公众号消息"}, button = "推送")
    @PostMapping("/pushGZHmsg")
    public Object pushGZHmsg(@RequestBody String body){
    	String orderSn = JacksonUtil.parseString(body, "orderSn");
    	String msg = JacksonUtil.parseString(body, "msg");
    	String openId = JacksonUtil.parseString(body, "openId");
    	//wxMpMsgTemplateService.manualSend(orderSn,msg);
    	return wxMpMsgTemplateService.manualSend(orderSn,msg,openId);
    }
    
}
