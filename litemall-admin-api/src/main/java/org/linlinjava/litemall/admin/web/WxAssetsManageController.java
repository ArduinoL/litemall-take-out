package org.linlinjava.litemall.admin.web;

import java.util.List;

import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.service.WxAssetsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;
import me.chanjar.weixin.common.error.WxErrorException;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialFileBatchGetResult;
import me.chanjar.weixin.mp.bean.material.WxMpMaterialUploadResult;
import me.chanjar.weixin.mp.bean.material.WxMpNewsArticle;

@RequiredArgsConstructor
@RestController
@RequestMapping("/admin/wxAssets")
@Validated
public class WxAssetsManageController {

	@Autowired
	private WxAssetsService wxAssetsService;
	
	@GetMapping("/list")
	public Object materialCount() throws WxErrorException {
		return wxAssetsService.materialCount();
	}
	
    /**
     * 根据类别分页获取非图文素材列表
     *
     * @param type
     * @param page
     * @return
     * @throws WxErrorException
     */
    @GetMapping("/materialFileBatchGet")
    public Object materialFileBatchGet() throws WxErrorException {
    	String type = "image";
    	int page = 1;
        WxMpMaterialFileBatchGetResult res = wxAssetsService.materialFileBatchGet(type,page);
        return ResponseUtil.ok(res);
    }
    
    /**
     * 添加图文永久素材
     *
     * @param articles
     * @return
     * @throws WxErrorException
     */
    @PostMapping("/materialNewsUpload")
    public Object materialNewsUpload(@RequestBody List<WxMpNewsArticle> articles) throws WxErrorException {
        if(articles.isEmpty()) {
            return ResponseUtil.badArgument();
        }
        WxMpMaterialUploadResult res = wxAssetsService.materialNewsUpload(articles);
        return ResponseUtil.ok();
    }
}
