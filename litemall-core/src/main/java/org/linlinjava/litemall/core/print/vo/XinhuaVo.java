package org.linlinjava.litemall.core.print.vo;

public class XinhuaVo{

	/**
	 * 打印机名称
	 */
	private String name;
	
	/**
	 * 打印机sn
	 */
	private String sn;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSn() {
		return sn;
	}

	public void setSn(String sn) {
		this.sn = sn;
	}

    
	
}
