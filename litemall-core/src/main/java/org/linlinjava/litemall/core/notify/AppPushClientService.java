package org.linlinjava.litemall.core.notify;

import java.util.List;

import org.linlinjava.litemall.db.domain.SysPushMsg;

public interface AppPushClientService {

    /**
     * 对单个用户推送消息
     *
     * @param msgPos
     * @return
     */
    public String pushToSingle(SysPushMsg msgPos);

    /**
     * 批量单推
     * <p>
     * 当单推任务较多时，推荐使用该接口，可以减少与服务端的交互次数。
     *
     * @param msgPos
     */
    public void pushToSingleBatch(List<SysPushMsg> msgPos);
}
