package org.linlinjava.litemall.core.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import me.chanjar.weixin.mp.api.WxMpService;
import me.chanjar.weixin.mp.api.impl.WxMpServiceImpl;
import me.chanjar.weixin.mp.config.impl.WxMpDefaultConfigImpl;


@Configuration
public class WxMpConfig {
	
    @Autowired
    private WxProperties properties;
    
    //公众号
    @Bean
    public WxMpDefaultConfigImpl config() {
        //WxMaInMemoryConfig config = new WxMaInMemoryConfig();
    	WxMpDefaultConfigImpl config = new WxMpDefaultConfigImpl();
    	config.setAppId(properties.getGzhAppId());
    	config.setSecret(properties.getGzhAppSecret());
    	config.setToken(properties.getGzhToken());
    	config.setAesKey(properties.getAppSecret());
        return config;
    }
    
    @Bean
    public WxMpService wxMpService(WxMpDefaultConfigImpl mpConfig) {
    	WxMpService service = new WxMpServiceImpl();
    	service.setWxMpConfigStorage(mpConfig);
        return service;
    }

}
