package org.linlinjava.litemall.core.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.notify.NotifyService;
import org.linlinjava.litemall.core.print.util.FeieService;
import org.linlinjava.litemall.core.print.util.XinhuaService;
import org.linlinjava.litemall.core.redis.RedisCache;
import org.linlinjava.litemall.core.util.BeanUtil;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.SysUser;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.linlinjava.litemall.db.service.SysUserService;
import org.linlinjava.litemall.db.util.OrderUtil;
import org.springframework.util.StringUtils;

import com.alibaba.fastjson.JSONObject;

import java.time.LocalDateTime;

public class OrderUnpaidTask2 extends printSnTask {
    private final Log logger = LogFactory.getLog(OrderUnpaidTask2.class);
	private String orderSn = "";
	private String printOrder = "";
	

    public OrderUnpaidTask2(String orderSn, long delayInMilliseconds){
        super("OrderUnpaidTask2-" + orderSn, delayInMilliseconds);
        this.orderSn = orderSn;
    }

    public OrderUnpaidTask2(String orderSn,String printOrder,Integer delayInMilliseconds){
        super("OrderUnpaidTask2-" + orderSn, delayInMilliseconds * 60 * 1000);
        //this.orderId = orderId;
        this.orderSn  = orderSn;
        this.printOrder  = printOrder;
    }

    @Override
    public void run() {
        logger.info("系统开始处理延时任务---检验是否漏单---" + this.orderSn);
        LitemallOrderService orderService = BeanUtil.getBean(LitemallOrderService.class);
        RedisCache redisCache = BeanUtil.getBean(RedisCache.class);
        LitemallShopService shopService = BeanUtil.getBean(LitemallShopService.class);
        SysUserService userService = BeanUtil.getBean(SysUserService.class);

        LitemallOrder order = orderService.findBySn(this.orderSn);
        if(order == null){
            return;
        }
        //boolean result = FeieService.selectPrintState(order.getShipChannel());
        //如果订单已退款就不检测了
        if(order.getOrderStatus() == 203) {
        	logger.info("系统结束处理延时任务---订单已完成退款，无需检测---" + this.orderSn);
        	return;
        }
        String print = redisCache.getCacheObject("shop_print_" + order.getShopId() + "_" + order.getShipSn());
        if(!StringUtils.isEmpty(print)) {
        	
    		JSONObject feie = JSONObject.parseObject(print);
    		String s = (String) feie.get("data");
    		boolean result = false;
        	LitemallShop shopInfo = shopService.getOneByShopID(order.getShopId());
        	SysUser userInfo = userService.getOneById(shopInfo.getAdminId().longValue());
        	
        	if(userInfo.getPrintType() == 1) {//飞鹅打印机
        		result = FeieService.selectPrintState(s);
        	}else {
        		result = XinhuaService.selectPrintState(s);
        	}
        	
        	//如果商家打印机不在线，打不出单，就不修改订单状态，允许用户去退款
        	if(result) {
        		// 修改已接单
        		if(order.getOrderStatus() != OrderUtil.STATUS_PAY) {
        			order.setShipChannel(print);
        			order.setOrderStatus(OrderUtil.STATUS_MAKE);
                    order.setEndTime(LocalDateTime.now());
                    if (orderService.updateWithOptimisticLocker(order) == 0) {
                        throw new RuntimeException("更新数据已失效");
                    }
        		}
        	}
        	//删除redis缓存
        	redisCache.deleteObject("shop_print_" + order.getShopId() + "_" + order.getShipSn());
        	
            logger.info("系统结束处理延时任务---检验是否漏单---" + this.orderSn);
        }
        /*
        else {
        	NotifyService notifyService = BeanUtil.getBean(NotifyService.class);
        	//漏单，重新补发漏单信息到商家打印机
        	notifyService.pushPrintOrderInfo(order.getShopId(), order.getOrderSn(), order.getShipSn());
        	logger.info("系统结束处理延时任务---检验是否漏单---" + this.orderSn);
        }*/

        
    }
}
