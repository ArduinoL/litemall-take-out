package org.linlinjava.litemall.core.print.util;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.linlinjava.litemall.core.redis.RedisCache;
import org.linlinjava.litemall.core.util.BeanUtil;
import org.linlinjava.litemall.core.util.HttpUtil;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.domain.LitemallOrderGoods;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.service.LitemallOrderService;

import com.gexin.fastjson.JSONObject;

public class FeieService {

	public static final String URL = "http://api.feieyun.cn/Api/Open/";//不需要修改
	
	public static final String USER = "1046837749@qq.com";//*必填*：账号名
	public static final String UKEY = "BMaDTPDJAQKCDShe";//*必填*: 飞鹅云后台注册账号后生成的UKEY 【备注：这不是填打印机的KEY】
	public static final String SN = "921562358";//*必填*：打印机编号，必须要在管理后台里添加打印机或调用API接口添加之后，才能调用API
	
	/**
	 * 绑定云打印机
	 * @param snlist
	 * @return
	 */
	public static List<String> binding(String snlist) {
		//String snlist = "921562358#subnkhnj#测试打印机123";
		String method = addprinter(snlist);
		JSONObject feie = JSONObject.parseObject(method);
		//System.out.println(JSONObject.parseObject(feie.getString("data")).get("ok"));
		List<String> s = (List<String>) JSONObject.parseObject(feie.getString("data")).get("ok");
		return s;
	}
	
	public static String printOrderInfo(String sn,LitemallOrder orderDetail,List<LitemallOrderGoods> orderGoodsList,LitemallShop shop,String shipSn){
		String method = print(sn,orderDetail,orderGoodsList,shop,shipSn);
		//JSONObject feie = JSONObject.parseObject(method);
		//String s = (String) feie.get("data");
		/*
		//查询订单打印
		String method1 = queryOrderState(s);
		JSONObject orderState = JSONObject.parseObject(method1);
		boolean state = (boolean) orderState.get("data");
		return state;*/
		return method;
	}
	
	public static String printRefundOrderInfo(String sn,LitemallOrder orderDetail,LitemallShop shop) {
		String method = printRefundInfo(sn,orderDetail,shop);
		JSONObject feie = JSONObject.parseObject(method);
		String s = (String) feie.get("data");
		return s;
	}
	
	public static boolean selectPrintState(String orderSn) {
		String method1 = queryOrderState(orderSn);
		JSONObject orderState = JSONObject.parseObject(method1);
		boolean state = (boolean) orderState.get("data");
		return state;
	}
	
    static class CallableTask implements Callable{
        @Override
        public String call() throws Exception {
            return "正在有返回值的多线程任务......";
        }
    }
    
	  private static String print(String sn,LitemallOrder orderDetail,List<LitemallOrderGoods> orderGoodsList,LitemallShop shop,String shipSn) {
	    String content = getOrder(orderDetail,orderGoodsList,shop,shipSn, 21, 7, 3);//orderList为数组 b1代表名称列占用（14个字节）  b2单价列（6个字节） b3数量列（3个字节） b4金额列（6个字节）-->这里的字节数可按自己需求自由改写，14+6+3+6再加上代码写的3个空格就是32了，58mm打印机一行总占32字节

	    String STIME = String.valueOf(System.currentTimeMillis() / 1000);
	    Map<String, String> params = new HashMap<>();
	    params.put("user", USER);
	    params.put("stime", STIME);
	    params.put("sig", signature(USER, UKEY, STIME));
	    params.put("apiname", "Open_printMsg");
	    
	    params.put("sn", sn);
	    params.put("content", content);
	    params.put("times", "1");
	    
	    
	    String result = HttpUtil.sendPost(URL, params);
	    
	    RedisCache redisCache = BeanUtil.getBean(RedisCache.class);
	    //将打印返回值存到缓存
    	redisCache.setCacheObject("shop_print_" + orderDetail.getShopId() + "_" + shipSn, result);
    	//orderDetail.setShipChannel(result);
    	//将返回值存入表
    	//LitemallOrderService OrderService = BeanUtil.getBean(LitemallOrderService.class);
    	//LitemallOrder o = new LitemallOrder();
    	//o.setId(orderDetail.getId());
    	//o.setShipChannel(result);
    	//int res = OrderService.updataOrderPrintInfo(orderDetail);
    	
	    System.out.println("飞鹅--商家"+ shop.getId() +"返回值：" + result);
	    
	    return result;
	  }
	  
	  
	  private static String printRefundInfo(String sn,LitemallOrder orderDetail,LitemallShop shop) {
		    String content = getRefundOrder(orderDetail,shop);//orderList为数组 b1代表名称列占用（14个字节）  b2单价列（6个字节） b3数量列（3个字节） b4金额列（6个字节）-->这里的字节数可按自己需求自由改写，14+6+3+6再加上代码写的3个空格就是32了，58mm打印机一行总占32字节

		    // 通过POST请求，发送打印信息到服务器
		    RequestConfig requestConfig = RequestConfig.custom()
		        .setSocketTimeout(30000)// 读取超时
		        .setConnectTimeout(30000)// 连接超时
		        .build();

		    CloseableHttpClient httpClient = HttpClients.custom().setDefaultRequestConfig(requestConfig).build();
		    HttpPost post = new HttpPost(URL);
		    List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		    nvps.add(new BasicNameValuePair("user", USER));
		    String STIME = String.valueOf(System.currentTimeMillis() / 1000);
		    nvps.add(new BasicNameValuePair("stime", STIME));
		    nvps.add(new BasicNameValuePair("sig", signature(USER, UKEY, STIME)));
		    nvps.add(new BasicNameValuePair("apiname", "Open_printMsg"));// 固定值,不需要修改
		    nvps.add(new BasicNameValuePair("sn", sn));
		    nvps.add(new BasicNameValuePair("content", content));
		    nvps.add(new BasicNameValuePair("times", "1"));// 打印联数
		    
		    CloseableHttpResponse response = null;
		    String result = null;
		    try {
		      post.setEntity(new UrlEncodedFormEntity(nvps, "utf-8"));
		      response = httpClient.execute(post);
		      int statecode = response.getStatusLine().getStatusCode();
		      if (statecode == 200) {
		        HttpEntity httpentity = response.getEntity();
		        if (httpentity != null) {
		          // 服务器返回的JSON字符串，建议要当做日志记录起来
		          result = EntityUtils.toString(httpentity);
		        }
		      }
		    } catch (Exception e) {
		      e.printStackTrace();
		    } finally {
		      close(response, post, httpClient);
		    }
		    return result;
	  }
	  
	    public void writeFile(String path,String content) {
	      content = new SimpleDateFormat("yyyy年MM月dd日 HH时mm分ss秒").format(new Date()) +",保存的订单日志信息为: "+content;
	        FileOutputStream fos = null;
	        try {
	            fos = new FileOutputStream(path, true);  
	            fos.write(content.getBytes());
	            fos.write("\r<BR>".getBytes());
	        } catch (IOException e) {
	            e.printStackTrace();
	        }finally{
	            if(fos != null){
	                try {
	                    fos.flush();
	                    fos.close(); 
	                } catch (IOException e) {
	                    e.printStackTrace();
	                }
	            }
	        }
	    }
	
	    
	    public static String getOrder(LitemallOrder orderDetail,List<LitemallOrderGoods> orderGoodsList, LitemallShop shop, String shipSn, int b1, int b2, int b3) {
	        
	    	//DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	    	//df.format(orderDetail.getPayTime());
	    	/*
	    	String Sn = orderDetail.getShipSn();
	    	if(StringUtils.isEmpty(Sn)) {
	    		Sn = shipSn;
	    	}*/
	    	
	    	String orderInfo = "<CB>"+ shipSn +"快当外卖</CB><BR>";
	        orderInfo += "【"+shop.getShopName()+"】<BR>";
	        orderInfo += "单号："+orderDetail.getOrderSn()+"<BR><BR>";
	        /*
	        LocalDateTime locatDate = LocalDateTime.now();
	        if(orderDetail.getPayTime() != null) {
	        	locatDate = orderDetail.getPayTime();
	        }*/
	        //时间偶尔获取不到。。。 LocalDateTime.now()
	        //orderInfo += "下单时间："+DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(locatDate)+"<BR><BR>";
	        orderInfo += "名称                  单价  数量<BR>";
	        //orderInfo += "--------------------------------<BR>";
	        orderInfo += "********************************<BR>";
	        //double totals = 0.0;
	        int overlength = 0;
	        for (int i = 0; i < orderGoodsList.size(); i++) {
	          String title = orderGoodsList.get(i).getGoodsName();
	          String price = orderGoodsList.get(i).getPrice().toString();
	          String num = orderGoodsList.get(i).getNumber().toString();
	          //String total = "" + Double.valueOf(price) * Integer.parseInt(num);
	          //totals += Double.parseDouble(total);
	          price = addSpace(price, b2);
	          num = addSpace(num, b3);
	          //total = p.addSpace(total, b4);
	          String otherStr =  "<L>"+price;
	          otherStr += "x"+num;
	          otherStr += "</L>";

	          int tl = 0;
	          try {
	            tl = title.getBytes("GBK").length;
	          } catch (UnsupportedEncodingException e) {
	            e.printStackTrace();
	          }
	          
	          int spaceNum = (tl / b1 + 1) * b1 - tl;
	          if (tl < b1) {
	            for (int k = 0; k < spaceNum; k++) {
	              title += " ";
	            }
	            title = "<L>"+title+"</L><BR>";
	            //title += otherStr;
	          } else if (tl == b1) {
	        	title = "<L>"+title+"</L><BR>";
	            //title += otherStr;
	          } else {
	        	overlength = 1;
	            List<String> list = null;
	            if (isEn(title)) {
	              list = getStrList(title, b1);
	            } else {
	              list = getStrList(title, b1 / 2);
	            }
	            String s0 = titleAddSpace(list.get(0));
	            title = s0;
	            //title = "<B>"+s0+"</B>";
	            //title = title + otherStr + "<BR>";// 添加 单价 数量 总额
	            String s = "";
	            for (int k = 1; k < list.size(); k++) {
	              s += list.get(k);
	            }

	            title += s;
	            title = "<L>"+title+"</L><BR>";

	            //title = title + "<RIGHT>"+ otherStr + "</RIGHT>";// 添加 单价 数量 总额
	          }
	          
	          

	          
	          //"<C><BOLD>"+title+"</BOLD></C><BR>";
//	        String s6 = "<C>默认不加标签最小效果</C><BR>";
//	        String s7 = "<C><L><BOLD>变高一倍加粗</BOLD></L></C><BR>";
	          if(overlength == 0) {
	        	  orderInfo += "<BOLD>"+title+"</BOLD>";
	          }
	          
	          String[] ssList = orderGoodsList.get(i).getSpecifications();
	          String guige = "[";
	          String ss = "";
	          for (int jj = 0; jj < ssList.length; jj++) {
	        	  guige += ssList[jj];
	            }
	          guige += "]";
	          
	          if(!"[标准]".equals(guige)) {
	        	  //String guige = orderGoodsList.get(i).getSpecifications();
	        	  List<String> lists = getStrList(guige, b1 / 2);
	        	  for (int kk = 0; kk < lists.size(); kk++) {
	        		  ss += lists.get(kk);
	        	  }
	        	  try {
	        		  ss = getStringByEnter(b1, ss);
	        	  } catch (Exception e) {
	        		  // TODO Auto-generated catch block
	        		  e.printStackTrace();
	        	  }
	        	  
	          }
        	  //规格
        	  if(overlength == 0) {
        		  orderInfo += ss+"<BR>" + "<RIGHT>"+ otherStr + "</RIGHT>";
        	  }else {
        		  orderInfo += "<BOLD>"+title+"</BOLD>" + ss + "<BR>" + "<RIGHT>"+ otherStr + "</RIGHT>";
        	  }
	          //orderInfo += "<BR>";
	        }
	        orderInfo += "<BR>";
	        orderInfo += "--------------------------------<BR>";
	        orderInfo += "<B>备注："+orderDetail.getMessage()+"</B><BR>";
	        orderInfo += "--------------------------------<BR>";
	        orderInfo += "配送费：" + orderDetail.getFreightPrice() + "元<BR>";
	        orderInfo += "打包费：" + orderDetail.getPackPrcie() + "元<BR>";
	        
	        orderInfo += "优惠金额：" + orderDetail.getCouponPrice() + "元<BR>";
	        orderInfo += "原价：" + orderDetail.getOrderPrice() + "元<BR>";
	        orderInfo += "<B><BOLD>实付：" + orderDetail.getActualPrice() + "元</BOLD></B><BR>";
	        
	        orderInfo += "--------------------------------<BR>";
	        String str = orderDetail.getDeliverystatus()==0?"侧门自取":"配送宿舍";
	        orderInfo += "配送宿舍：<BR>";
	        orderInfo += "<B>"+str+"</B><BR>";
	        orderInfo += "--------------------------------<BR>";
	        orderInfo += "<B>"+orderDetail.getConsignee()+"</B><BR>";
	        orderInfo += "<B>"+orderDetail.getMobile()+"</B><BR>";
	        orderInfo += "<B>"+orderDetail.getAddress()+"</B><BR><BR>";
	        orderInfo += "<QR>"+orderDetail.getOrderSn()+"</QR>";
	        orderInfo += "打印时间：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "<BR>";
	        orderInfo += "商家电话：" + shop.getShopPhone() + "<BR>";
	        orderInfo += "投诉电话：19195736320" + "<BR><BR><BR>";
	        return orderInfo;
	      }
	    
	    
	    public static String getRefundOrder(LitemallOrder orderDetail,LitemallShop shop) {
	    	
	    	String orderInfo = "<CB>"+orderDetail.getShipSn()+"快当外卖</CB><BR>";
	        orderInfo += "【"+shop.getShopName()+"】<BR>";
	        orderInfo += "单号："+orderDetail.getOrderSn()+"<BR>";
	        //时间偶尔获取不到。。。 new DATA
	        orderInfo += "下单时间："+DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss").format(orderDetail.getPayTime())+"<BR><BR>";
	        
	        orderInfo += "--------------------------------<BR>";
	        orderInfo += "<B>用户取消订单</B><BR>";
	        orderInfo += "<B>系统已退款给用户</B><BR>";
	        orderInfo += "<B>订单编号："+orderDetail.getShipSn()+"</B><BR>";
	        orderInfo += "--------------------------------<BR>";
	        
	        orderInfo += "打印时间：" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "<BR>";
	        orderInfo += "<BR><BR><BR>";
	        return orderInfo;
	    }
	    
	    public static String titleAddSpace(String str) {
	        int k=0;
	        int b = 14;
	        try {
	          k = str.getBytes("GBK").length;
	        } catch (UnsupportedEncodingException e) {
	          e.printStackTrace();
	        }
	        for (int i = 0; i < b-k; i++) {
	          str += " ";
	        }
	        return str;
	      }
	      
	      public static String getStringByEnter(int length, String string) throws Exception {
	        for (int i = 1; i <= string.length(); i++) {
	          if (string.substring(0, i).getBytes("GBK").length > length) {
	            return string.substring(0, i - 1) + "<BR>" + getStringByEnter(length, string.substring(i - 1));
	          }
	        }
	        return string;
	      }

	      public static String addSpace(String str, int size) {
	        int len = str.length();
	        if (len < size) {
	          for (int i = 0; i < size - len; i++) {
	            str += " ";
	          }
	        }
	        return str;
	      }
	      
	      public static Boolean isEn(String str) {
	        Boolean b = false;
	        try {
	          b = str.getBytes("GBK").length == str.length();
	        } catch (UnsupportedEncodingException e) {
	          e.printStackTrace();
	        }
	        return b;
	      }
	      
	      public static List<String> getStrList(String inputString, int length) {
	        int size = inputString.length() / length;
	        if (inputString.length() % length != 0) {
	          size += 1;
	        }
	        return getStrList(inputString, length, size);
	      }

	      public static List<String> getStrList(String inputString, int length, int size) {
	        List<String> list = new ArrayList<String>();
	        for (int index = 0; index < size; index++) {
	          String childStr = substring(inputString, index * length, (index + 1) * length);
	          list.add(childStr);
	        }
	        return list;
	      }

	      public static String substring(String str, int f, int t) {
	        if (f > str.length())
	          return null;
	        if (t > str.length()) {
	          return str.substring(f, str.length());
	        } else {
	          return str.substring(f, t);
	        }
	      }

	      public static void close(CloseableHttpResponse response, HttpPost post, CloseableHttpClient httpClient) {
	        try {
	          if (response != null) {
	            response.close();
	          }
	        } catch (IOException e) {
	          e.printStackTrace();
	        }
	        try {
	          post.abort();
	        } catch (Exception e) {
	          e.printStackTrace();
	        }
	        try {
	          httpClient.close();
	        } catch (IOException e) {
	          e.printStackTrace();
	        }
	      }

	//**********测试时，打开下面注释掉方法的即可,更多接口文档信息,请访问官网开放平台查看**********
	public static void main(String[] args) throws Exception{
		
		//==================添加打印机接口（支持批量）==================
		//***返回值JSON字符串***
		//正确例子：{"msg":"ok","ret":0,"data":{"ok":["sn#key#remark#carnum","316500011#abcdefgh#快餐前台"],"no":["316500012#abcdefgh#快餐前台#13688889999  （错误：识别码不正确）"]},"serverExecutedTime":3}
		//错误：{"msg":"参数错误 : 该帐号未注册.","ret":-2,"data":null,"serverExecutedTime":37}

		//提示：打印机编号(必填) # 打印机识别码(必填) # 备注名称(选填) # 流量卡号码(选填)，多台打印机请换行（\n）添加新打印机信息，每次最多100行(台)。
		/*
		String snlist = "921562358#subnkhnj#测试打印机123";
		String method = addprinter(snlist);
		JSONObject feie = JSONObject.parseObject(method);
		System.out.println(feie.get("msg"));*/
		
		
		
		//==================方法1.小票机打印订单接口==================
		//***返回值JSON字符串***
		//成功：{"msg":"ok","ret":0,"data":"xxxxxxx_xxxxxxxx_xxxxxxxx","serverExecutedTime":5}
		//失败：{"msg":"错误描述","ret":非0,"data":"null","serverExecutedTime":5}
		
		//String method1 = print(SN);//该接口只能是小票机使用,如购买的是标签机请使用下面方法2，调用打印
		//System.out.println(method1);

		
		
		//==================方法2.标签机专用打印订单接口==================
		//***返回值JSON字符串***
		//成功：{"msg":"ok","ret":0,"data":"xxxxxxx_xxxxxxxx_xxxxxxxx","serverExecutedTime":5}
		//失败：{"msg":"错误描述","ret":非0,"data":"null","serverExecutedTime":5}
		
//		String method2 = printLabelMsg(SN);//打开注释调用标签机打印接口进行打印,该接口只能是标签机使用，其它型号打印机请勿使用该接口
//		System.out.println(method2);
		
		
		
		//===========方法3.查询某订单是否打印成功=============
		//***返回值JSON字符串***
	 	//成功：{"msg":"ok","ret":0,"data":true,"serverExecutedTime":2}//data:true为已打印,false为未打印
		//失败：{"msg":"错误描述","ret":非0, "data":null,"serverExecutedTime":7}
		
//		String orderid = "xxxxxxx_xxxxxxxx_xxxxxxxx";//订单ID，从方法1返回值data获取
//		String method3 = queryOrderState(orderid);
//		System.out.println(method3);
		
		
		
		//===========方法4.查询指定打印机某天的订单详情============
		//***返回值JSON字符串***
		//成功：{"msg":"ok","ret":0,"data":{"print":6,"waiting":1},"serverExecutedTime":9}//print已打印，waiting为打印
		//失败：{"msg":"错误描述","ret":非0,"data":"null","serverExecutedTime":5}
		
//		String strdate = "2016-11-12";//注意时间格式为"yyyy-MM-dd"
//		String method4 = queryOrderInfoByDate(SN,strdate);
//		System.out.println(method4);
		
		
		
		//===========方法5.查询打印机的状态==========================
		//***返回值JSON字符串***
	    //成功：{"msg":"ok","ret":0,"data":"状态","serverExecutedTime":4}
		//失败：{"msg":"错误描述","ret":非0,"data":"null","serverExecutedTime":5}
		
//		String method5 = queryPrinterStatus(SN);
//		System.out.println(method5);
		
	}
	
	
	
	
	
	//=====================以下是函数实现部分================================================
	
	private static String addprinter(String snlist){
		
	   //通过POST请求，发送打印信息到服务器
		RequestConfig requestConfig = RequestConfig.custom()  
	            .setSocketTimeout(30000)//读取超时  
	            .setConnectTimeout(30000)//连接超时
	            .build();
		
		CloseableHttpClient httpClient = HttpClients.custom()
				 .setDefaultRequestConfig(requestConfig)
				 .build();	
		
	    HttpPost post = new HttpPost(URL);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user",USER));
		String STIME = String.valueOf(System.currentTimeMillis()/1000);
		nvps.add(new BasicNameValuePair("stime",STIME));
		nvps.add(new BasicNameValuePair("sig",signature(USER,UKEY,STIME)));
		nvps.add(new BasicNameValuePair("apiname","Open_printerAddlist"));//固定值,不需要修改
		nvps.add(new BasicNameValuePair("printerContent",snlist));
		
		CloseableHttpResponse response = null;
		String result = null;
        try
        {
    	   post.setEntity(new UrlEncodedFormEntity(nvps,"utf-8"));
    	   response = httpClient.execute(post);
       	   int statecode = response.getStatusLine().getStatusCode();
       	   if(statecode == 200){
	        	HttpEntity httpentity = response.getEntity(); 
	            if (httpentity != null){
	            	result = EntityUtils.toString(httpentity);
	            }
           }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       }
       finally{
    	   try {
    		   if(response!=null){
    			   response.close();
    		   }
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
    	   try {
    		   post.abort();
    	   } catch (Exception e) {
    		   e.printStackTrace();
    	   }
    	   try {
    		   httpClient.close();
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
       }
       return result;
	  
	}
			
			
	//方法1
	private static String print(String sn){
		//标签说明：
		//单标签: 
		//"<BR>"为换行,"<CUT>"为切刀指令(主动切纸,仅限切刀打印机使用才有效果)
		//"<LOGO>"为打印LOGO指令(前提是预先在机器内置LOGO图片),"<PLUGIN>"为钱箱或者外置音响指令
		//成对标签：
		//"<CB></CB>"为居中放大一倍,"<B></B>"为放大一倍,"<C></C>"为居中,<L></L>字体变高一倍
		//<W></W>字体变宽一倍,"<QR></QR>"为二维码,"<BOLD></BOLD>"为字体加粗,"<RIGHT></RIGHT>"为右对齐
		//拼凑订单内容时可参考如下格式
		//根据打印纸张的宽度，自行调整内容的格式，可参考下面的样例格式
		
		String content;
		content = "<CB>快当外卖</CB><BR>";
		content += "【快当商店】<BR>";
		content += "单号：2021040712334465<BR>";
		content += "时间：2021-04-07 16:11:02<BR>";
		content += "名称　　　　　 单价  数量 金额<BR>";
		content += "--------------------------------<BR>";
		content += "饭　　　　　　 1.0    1   1.0<BR>";
		content += "炒饭　　　　　 10.0   10  10.0<BR>";
		content += "蛋炒饭　　　　 10.0   10  100.0<BR>";
		content += "鸡蛋炒饭　　　 100.0  1   100.0<BR>";
		content += "番茄蛋炒饭　　 1000.0 1   100.0<BR>";
		content += "西红柿蛋炒饭　 1000.0 1   100.0<BR>";
		content += "西红柿鸡蛋炒饭西红柿鸡蛋炒饭  100.0  10  100.0<BR>";
		content += "备注：加辣<BR>";
		content += "--------------------------------<BR>";
		content += "合计：xx.0元<BR>";
		content += "送货地点：广州市南沙区xx路xx号<BR>";
		content += "联系电话：13888888888888<BR>";
		content += "订餐时间：2016-08-08 08:08:08<BR>";
		content += "<QR>http://www.dzist.com</QR>";
		
	   //通过POST请求，发送打印信息到服务器
		RequestConfig requestConfig = RequestConfig.custom()  
	            .setSocketTimeout(30000)//读取超时  
	            .setConnectTimeout(30000)//连接超时
	            .build();
		
		CloseableHttpClient httpClient = HttpClients.custom()
				 .setDefaultRequestConfig(requestConfig)
				 .build();	
		
	    HttpPost post = new HttpPost(URL);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user",USER));
		String STIME = String.valueOf(System.currentTimeMillis()/1000);
		nvps.add(new BasicNameValuePair("stime",STIME));
		nvps.add(new BasicNameValuePair("sig",signature(USER,UKEY,STIME)));
		nvps.add(new BasicNameValuePair("apiname","Open_printMsg"));//固定值,不需要修改
		nvps.add(new BasicNameValuePair("sn",sn));
		nvps.add(new BasicNameValuePair("content",content));
		nvps.add(new BasicNameValuePair("times","1"));//打印联数
		
		CloseableHttpResponse response = null;
		String result = null;
        try
        {
    	   post.setEntity(new UrlEncodedFormEntity(nvps,"utf-8"));
    	   response = httpClient.execute(post);
       	   int statecode = response.getStatusLine().getStatusCode();
       	   if(statecode == 200){
	        	HttpEntity httpentity = response.getEntity(); 
	            if (httpentity != null){
	            	//服务器返回的JSON字符串，建议要当做日志记录起来
	            	result = EntityUtils.toString(httpentity);
	            }
           }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       }
       finally{
    	   try {
    		   if(response!=null){
    			   response.close();
    		   }
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
    	   try {
    		   post.abort();
    	   } catch (Exception e) {
    		   e.printStackTrace();
    	   }
    	   try {
    		   httpClient.close();
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
       }
       return result;
	  
	}
	
	
	
	//方法2
	private static String printLabelMsg(String sn){
		
		String content;
		content = "<DIRECTION>1</DIRECTION>";//设定打印时出纸和打印字体的方向，n 0 或 1，每次设备重启后都会初始化为 0 值设置，1：正向出纸，0：反向出纸，
		content += "<TEXT x='9' y='10' font='12' w='1' h='2' r='0'>#001       五号桌      1/3</TEXT><TEXT x='80' y='80' font='12' w='2' h='2' r='0'>可乐鸡翅</TEXT><TEXT x='9' y='180' font='12' w='1' h='1' r='0'>张三先生       13800138000</TEXT>";//40mm宽度标签纸打印例子，打开注释调用标签打印接口打印
		
	   //通过POST请求，发送打印信息到服务器
		RequestConfig requestConfig = RequestConfig.custom()  
	            .setSocketTimeout(30000)//读取超时  
	            .setConnectTimeout(30000)//连接超时
	            .build();
		
		CloseableHttpClient httpClient = HttpClients.custom()
				 .setDefaultRequestConfig(requestConfig)
				 .build();	
		
	    HttpPost post = new HttpPost(URL);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user",USER));
		String STIME = String.valueOf(System.currentTimeMillis()/1000);
		nvps.add(new BasicNameValuePair("stime",STIME));
		nvps.add(new BasicNameValuePair("sig",signature(USER,UKEY,STIME)));
		nvps.add(new BasicNameValuePair("apiname","Open_printLabelMsg"));//固定值,不需要修改
		nvps.add(new BasicNameValuePair("sn",sn));
		nvps.add(new BasicNameValuePair("content",content));
		nvps.add(new BasicNameValuePair("times","1"));//打印联数
		
		CloseableHttpResponse response = null;
		String result = null;
        try
        {
    	   post.setEntity(new UrlEncodedFormEntity(nvps,"utf-8"));
    	   response = httpClient.execute(post);
       	   int statecode = response.getStatusLine().getStatusCode();
       	   if(statecode == 200){
	        	HttpEntity httpentity = response.getEntity(); 
	            if (httpentity != null){
	            	//服务器返回的JSON字符串，建议要当做日志记录起来
	            	result = EntityUtils.toString(httpentity);
	            }
           }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       }
       finally{
    	   try {
    		   if(response!=null){
    			   response.close();
    		   }
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
    	   try {
    		   post.abort();
    	   } catch (Exception e) {
    		   e.printStackTrace();
    	   }
    	   try {
    		   httpClient.close();
    	   } catch (IOException e) {
    		   e.printStackTrace();
    	   }
       }
       return result;
	  
	}

	
	//方法3
	private static String queryOrderState(String orderid){
	
	   //通过POST请求，发送打印信息到服务器
		RequestConfig requestConfig = RequestConfig.custom()  
	            .setSocketTimeout(30000)//读取超时  
	            .setConnectTimeout(30000)//连接超时
	            .build();
		
		CloseableHttpClient httpClient = HttpClients.custom()
				 .setDefaultRequestConfig(requestConfig)
				 .build();	
		
	    HttpPost post = new HttpPost(URL);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user",USER));
		String STIME = String.valueOf(System.currentTimeMillis()/1000);
		nvps.add(new BasicNameValuePair("stime",STIME));
		nvps.add(new BasicNameValuePair("sig",signature(USER,UKEY,STIME)));
		nvps.add(new BasicNameValuePair("apiname","Open_queryOrderState"));//固定值,不需要修改
		nvps.add(new BasicNameValuePair("orderid",orderid));
		
		CloseableHttpResponse response = null;
		String result = null;
        try
        {
    	   post.setEntity(new UrlEncodedFormEntity(nvps,"utf-8"));
       	   response = httpClient.execute(post);
       	   int statecode = response.getStatusLine().getStatusCode();
       	   if(statecode == 200){
	        	HttpEntity httpentity = response.getEntity(); 
	            if (httpentity != null){
	            	//服务器返回
	            	result = EntityUtils.toString(httpentity);
	            }
           }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       }
        finally{
	    	   try {
	    		   if(response!=null){
	    			   response.close();
	    		   }
	    	   } catch (IOException e) {
	    		   e.printStackTrace();
	    	   }
	    	   try {
	    		   post.abort();
	    	   } catch (Exception e) {
	    		   e.printStackTrace();
	    	   }
	    	   try {
	    		   httpClient.close();
	    	   } catch (IOException e) {
	    		   e.printStackTrace();
	    	   }
	       }
       return result;
  
   }

	
	
	//方法4
	private static String queryOrderInfoByDate(String sn,String strdate){
	
	    //通过POST请求，发送打印信息到服务器
		RequestConfig requestConfig = RequestConfig.custom()  
	            .setSocketTimeout(30000)//读取超时  
	            .setConnectTimeout(30000)//连接超时
	            .build();
		
		CloseableHttpClient httpClient = HttpClients.custom()
				 .setDefaultRequestConfig(requestConfig)
				 .build();	
		
	    HttpPost post = new HttpPost(URL);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user",USER));
		String STIME = String.valueOf(System.currentTimeMillis()/1000);
		nvps.add(new BasicNameValuePair("stime",STIME));
		nvps.add(new BasicNameValuePair("sig",signature(USER,UKEY,STIME)));
		nvps.add(new BasicNameValuePair("apiname","Open_queryOrderInfoByDate"));//固定值,不需要修改
		nvps.add(new BasicNameValuePair("sn",sn));
		nvps.add(new BasicNameValuePair("date",strdate));//yyyy-MM-dd格式
		
		CloseableHttpResponse response = null;
		String result = null;
        try
        {
    	   post.setEntity(new UrlEncodedFormEntity(nvps,"utf-8"));
       	   response = httpClient.execute(post);
       	   int statecode = response.getStatusLine().getStatusCode();
       	   if(statecode == 200){
	        	HttpEntity httpentity = response.getEntity(); 
	            if (httpentity != null){
	            	//服务器返回
	            	result = EntityUtils.toString(httpentity);
	            }
           }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       }
        finally{
	    	   try {
	    		   if(response!=null){
	    			   response.close();
	    		   }
	    	   } catch (IOException e) {
	    		   e.printStackTrace();
	    	   }
	    	   try {
	    		   post.abort();
	    	   } catch (Exception e) {
	    		   e.printStackTrace();
	    	   }
	    	   try {
	    		   httpClient.close();
	    	   } catch (IOException e) {
	    		   e.printStackTrace();
	    	   }
	       }
       return result;
  
   }
	
	
	
	//方法5
	private static String queryPrinterStatus(String sn){
	
	    //通过POST请求，发送打印信息到服务器
		RequestConfig requestConfig = RequestConfig.custom()  
	            .setSocketTimeout(30000)//读取超时  
	            .setConnectTimeout(30000)//连接超时
	            .build();
		
		CloseableHttpClient httpClient = HttpClients.custom()
				 .setDefaultRequestConfig(requestConfig)
				 .build();	
		
	    HttpPost post = new HttpPost(URL);
        List<NameValuePair> nvps = new ArrayList<NameValuePair>();
		nvps.add(new BasicNameValuePair("user",USER));
		String STIME = String.valueOf(System.currentTimeMillis()/1000);
		nvps.add(new BasicNameValuePair("stime",STIME));
		nvps.add(new BasicNameValuePair("sig",signature(USER,UKEY,STIME)));
		nvps.add(new BasicNameValuePair("apiname","Open_queryPrinterStatus"));//固定值,不需要修改
		nvps.add(new BasicNameValuePair("sn",sn));
		
		CloseableHttpResponse response = null;
		String result = null;
        try
        {
    	   post.setEntity(new UrlEncodedFormEntity(nvps,"utf-8"));
       	   response = httpClient.execute(post);
       	   int statecode = response.getStatusLine().getStatusCode();
       	   if(statecode == 200){
	        	HttpEntity httpentity = response.getEntity(); 
	            if (httpentity != null){
	            	//服务器返回
	            	result = EntityUtils.toString(httpentity);
	            }
           }
       }
       catch (Exception e)
       {
    	   e.printStackTrace();
       }
        finally{
	    	   try {
	    		   if(response!=null){
	    			   response.close();
	    		   }
	    	   } catch (IOException e) {
	    		   e.printStackTrace();
	    	   }
	    	   try {
	    		   post.abort();
	    	   } catch (Exception e) {
	    		   e.printStackTrace();
	    	   }
	    	   try {
	    		   httpClient.close();
	    	   } catch (IOException e) {
	    		   e.printStackTrace();
	    	   }
	       }
       return result;
  
   }
	
	
	//生成签名字符串
	private static String signature(String USER,String UKEY,String STIME){
		String s = DigestUtils.sha1Hex(USER+UKEY+STIME);
		return s;
	}
}
