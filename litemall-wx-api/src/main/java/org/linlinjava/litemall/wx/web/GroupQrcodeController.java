package org.linlinjava.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.util.RegexUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallAddress;
import org.linlinjava.litemall.db.domain.LitemallGroupQrcode;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallShopSetting;
import org.linlinjava.litemall.db.service.LitemallAddressService;
import org.linlinjava.litemall.db.service.LitemallGroupQrCodeService;
import org.linlinjava.litemall.db.service.LitemallRegionService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.linlinjava.litemall.db.service.LitemallShopSettingService;
import org.linlinjava.litemall.db.util.HomeCacheManager;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.service.GetRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自提店铺
 */
@RestController
@RequestMapping("/wx/qr")
@Validated
public class GroupQrcodeController {
	private final Log logger = LogFactory.getLog(GroupQrcodeController.class);

	@Autowired
	private LitemallGroupQrCodeService GroupQrCodeService;


	@GetMapping("getQr")
	public Object getQr(String uuid) {
		if(StringUtils.isEmpty(uuid)) {
			return ResponseUtil.badArgument();
		}
        //优先从缓存中读取
        if (HomeCacheManager.hasData2(uuid.toString())) {
        	if(HomeCacheManager.getCacheData(uuid.toString()).get("data") != null) {
        		Object number = HomeCacheManager.getCacheData(uuid.toString()).get("number");
        		int n = (int) number;
        		n++;
        		Map<String, Object> entity = new HashMap<>();
        		entity.put("data", HomeCacheManager.getCacheData(uuid.toString()).get("data"));
        		entity.put("number", n);
        		HomeCacheManager.loadGoodsData(uuid.toString(), entity);
        		return ResponseUtil.ok(entity);
        	}
        }
        
        //uuid = "4631635e38bb4ceb92e396149a103eda";
		
		LitemallGroupQrcode groupQrcode = GroupQrCodeService.findByUUID(uuid);
		Map<String, Object> entity = new HashMap<>();
		entity.put("data", groupQrcode);
		entity.put("number", 1);
		HomeCacheManager.loadGoodsData(uuid.toString(), entity);
		return ResponseUtil.ok(entity);
	}




}