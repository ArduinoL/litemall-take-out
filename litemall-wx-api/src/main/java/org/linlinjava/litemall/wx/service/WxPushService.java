package org.linlinjava.litemall.wx.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Service;


import cn.binarywang.wx.miniapp.api.WxMaService;
import cn.binarywang.wx.miniapp.api.impl.WxMaServiceImpl;
import cn.binarywang.wx.miniapp.bean.WxMaSubscribeMessage;
import cn.binarywang.wx.miniapp.config.impl.WxMaDefaultConfigImpl;


@Configuration
@Service
public class WxPushService {
	
    
	public void notifyActivityUser(String ToUser) {
    	//WxMaSubscribeMessage subscribeMessage = wxPushService.su(order);
		WxMaSubscribeMessage subscribeMessage = new WxMaSubscribeMessage();
		subscribeMessage.setToUser(ToUser);
		subscribeMessage.setTemplateId("6D2k1y9GmfiIqd-PShZGu5YW2Xi-VwTH6LtKNsTcy4U");
		subscribeMessage.setPage("/pages/index/index");

		ArrayList<WxMaSubscribeMessage.Data> wxMaSubscribeData = new ArrayList<>();

//      订阅消息参数值内容限制说明
//            ---摘自微信小程序官方：https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
//      参数类别 	参数说明 	参数值限制 	   说明
//      thing.DATA 	事物 	20个以内字符 	可汉字、数字、字母或符号组合
//      number.DATA 	数字 	32位以内数字 	只能数字，可带小数
//      letter.DATA 	字母 	32位以内字母 	只能字母
//      symbol.DATA 	符号 	5位以内符号 	只能符号
//      character_string.DATA 	字符串 	32位以内数字、字母或符号 	可数字、字母或符号组合
//      time.DATA 	时间 	24小时制时间格式（支持+年月日） 	例如：15:01，或：2019年10月1日 15:01
//      date.DATA 	日期 	年月日格式（支持+24小时制时间） 	例如：2019年10月1日，或：2019年10月1日 15:01
//      amount.DATA 	金额 	1个币种符号+10位以内纯数字，可带小数，结尾可带“元” 	可带小数
//      phone_number.DATA 	电话 	17位以内，数字、符号 	电话号码，例：+86-0766-66888866
//      car_number.DATA 	车牌 	8位以内，第一位与最后一位可为汉字，其余为字母或数字 	车牌号码：粤A8Z888挂
//      name.DATA 	姓名 	10个以内纯汉字或20个以内纯字母或符号 	中文名10个汉字内；纯英文名20个字母内；中文和字母混合按中文名算，10个字内
//      phrase.DATA 	汉字 	5个以内汉字 	5个以内纯汉字，例如：配送中

		// 第一个内容： 邀请结果
		WxMaSubscribeMessage.Data wxMaSubscribeData1 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData1.setName("phrase2");
		wxMaSubscribeData1.setValue("成功");
		// 每个参数 存放到大集合中
		wxMaSubscribeData.add(wxMaSubscribeData1);

		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);

		// 第二个内容：时间
		WxMaSubscribeMessage.Data wxMaSubscribeData2 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData2.setName("time1");
		wxMaSubscribeData2.setValue(dateString);
		wxMaSubscribeData.add(wxMaSubscribeData2);

		// 第三个内容：温馨提醒
		WxMaSubscribeMessage.Data wxMaSubscribeData3 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData3.setName("thing5");
		wxMaSubscribeData3.setValue("您已点亮成功，进入小程序领取商品吧~");
		wxMaSubscribeData.add(wxMaSubscribeData3);

		// 把集合给大的data
		subscribeMessage.setData(wxMaSubscribeData);
		try {
			// 获取微信小程序配置：
			//final WxMaService wxService = WxMaAppServer.getMaService("wxdd15765cc615fe70");
	        WxMaDefaultConfigImpl config = new WxMaDefaultConfigImpl();
	        config.setAppid("wx9f4d90ddc3264074");
	        config.setSecret("b3a80e111a04b0950b472bc95d9f4d5c");
	        
	        WxMaService service = new WxMaServiceImpl();
	        service.setWxMaConfig(config);
	        service.getMsgService().sendSubscribeMsg(subscribeMessage);
			// log.info("【微信小程序推送订阅消息成功：】" + JsonUtils.toJson(subscribeMessage));
		} catch (Exception e) {
			// log.error("【微信小程序推送订阅消息失败：】" + JsonUtils.toJson(subscribeMessage));
			e.printStackTrace();
		}
	}
	

	/*
	public void wx_gzh_su() {
		// 切换公众号
		//wxMpService.switchoverTo("wxdd97f00beb4baefe");

		WxMpTemplateMessage wxMpTemplateMessage = new WxMpTemplateMessage();
		wxMpTemplateMessage.setTemplateId("7oyQ31n1531CnQMDVMvbwyqGqQ5xI6TKciwOTDdq1a4");
		wxMpTemplateMessage.setToUser("oV3-D0hnZOkhzPs1xwkOy_Dl5k-M");

		List<WxMpTemplateData> wxMpTemplateData = new ArrayList<>();

		// 标题
		WxMpTemplateData wxMpTemp = new WxMpTemplateData();
		wxMpTemp.setName("first");
		wxMpTemp.setValue("派单通知");
		wxMpTemplateData.add(wxMpTemp);

		// 下单日期
		WxMpTemplateData wxMpTemp1 = new WxMpTemplateData();
		wxMpTemp1.setName("keyword1");
		wxMpTemp1.setValue("2020.10.05 10：10：10");
		wxMpTemplateData.add(wxMpTemp1);

		// 服务类型
		WxMpTemplateData wxMpTemp2 = new WxMpTemplateData();
		wxMpTemp2.setName("keyword2");
		wxMpTemp2.setValue("代领快递");
		wxMpTemplateData.add(wxMpTemp2);

		wxMpTemplateMessage.setData(wxMpTemplateData);

		/*
		 * WxMpTemplateMessage.WxMpTemplateMessageBuilder builder =
		 * WxMpTemplateMessage.builder() .templateId("") .url("")
		 * .miniProgram(form.getMiniprogram()) .data(form.getData());
		 */
/*
		try {
			wxMpService.getTemplateMsgService().sendTemplateMsg(wxMpTemplateMessage);
		} catch (WxErrorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		*/
	//}
	
	/*
	public WxMaSubscribeMessage fail(LitemallOrder order) {
		
		//LitemallJtUser user = userService.findById(order.getUserId());
		
		WxMaSubscribeMessage subscribeMessage = new WxMaSubscribeMessage();
		subscribeMessage.setToUser(user.getPassword());
		subscribeMessage.setTemplateId("pmo3tUjA7NOrEcaeWi__8vfhX3K3b1fvfEXq1yJGYrg");
		subscribeMessage.setPage("");

		ArrayList<WxMaSubscribeMessage.Data> wxMaSubscribeData = new ArrayList<>();

//      订阅消息参数值内容限制说明
//            ---摘自微信小程序官方：https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
//      参数类别 	参数说明 	参数值限制 	   说明
//      thing.DATA 	事物 	20个以内字符 	可汉字、数字、字母或符号组合
//      number.DATA 	数字 	32位以内数字 	只能数字，可带小数
//      letter.DATA 	字母 	32位以内字母 	只能字母
//      symbol.DATA 	符号 	5位以内符号 	只能符号
//      character_string.DATA 	字符串 	32位以内数字、字母或符号 	可数字、字母或符号组合
//      time.DATA 	时间 	24小时制时间格式（支持+年月日） 	例如：15:01，或：2019年10月1日 15:01
//      date.DATA 	日期 	年月日格式（支持+24小时制时间） 	例如：2019年10月1日，或：2019年10月1日 15:01
//      amount.DATA 	金额 	1个币种符号+10位以内纯数字，可带小数，结尾可带“元” 	可带小数
//      phone_number.DATA 	电话 	17位以内，数字、符号 	电话号码，例：+86-0766-66888866
//      car_number.DATA 	车牌 	8位以内，第一位与最后一位可为汉字，其余为字母或数字 	车牌号码：粤A8Z888挂
//      name.DATA 	姓名 	10个以内纯汉字或20个以内纯字母或符号 	中文名10个汉字内；纯英文名20个字母内；中文和字母混合按中文名算，10个字内
//      phrase.DATA 	汉字 	5个以内汉字 	5个以内纯汉字，例如：配送中

		// 第一个内容： 订单信息
		WxMaSubscribeMessage.Data wxMaSubscribeData1 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData1.setName("character_string1");
		wxMaSubscribeData1.setValue(order.getOrderSn());
		// 每个参数 存放到大集合中
		wxMaSubscribeData.add(wxMaSubscribeData1);

		Date currentTime = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		String dateString = formatter.format(currentTime);

		// 第二个内容：时间
		WxMaSubscribeMessage.Data wxMaSubscribeData2 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData2.setName("time4");
		wxMaSubscribeData2.setValue(dateString);
		wxMaSubscribeData.add(wxMaSubscribeData2);

		// 第三个内容：原因
		WxMaSubscribeMessage.Data wxMaSubscribeData3 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData3.setName("thing5");
		wxMaSubscribeData3.setValue("无人接单,金额已退还到余额。");
		wxMaSubscribeData.add(wxMaSubscribeData3);

		// 把集合给大的data
		subscribeMessage.setData(wxMaSubscribeData);
		return subscribeMessage;
	}

	public WxMaSubscribeMessage su(LitemallOrder order) {
		LitemallJtUser user = userService.findById(order.getUserId());
		
		WxMaSubscribeMessage subscribeMessage = new WxMaSubscribeMessage();
		subscribeMessage.setToUser(user.getPassword());
		subscribeMessage.setTemplateId("lknq1suCrFVZWVmmYBwy82nl3LVAqGdMe3OPQN8H-xU");

		ArrayList<WxMaSubscribeMessage.Data> wxMaSubscribeData = new ArrayList<>();

//      订阅消息参数值内容限制说明
//            ---摘自微信小程序官方：https://developers.weixin.qq.com/miniprogram/dev/api-backend/open-api/subscribe-message/subscribeMessage.send.html
//      参数类别 	参数说明 	参数值限制 	   说明
//      thing.DATA 	事物 	20个以内字符 	可汉字、数字、字母或符号组合
//      number.DATA 	数字 	32位以内数字 	只能数字，可带小数
//      letter.DATA 	字母 	32位以内字母 	只能字母
//      symbol.DATA 	符号 	5位以内符号 	只能符号
//      character_string.DATA 	字符串 	32位以内数字、字母或符号 	可数字、字母或符号组合
//      time.DATA 	时间 	24小时制时间格式（支持+年月日） 	例如：15:01，或：2019年10月1日 15:01
//      date.DATA 	日期 	年月日格式（支持+24小时制时间） 	例如：2019年10月1日，或：2019年10月1日 15:01
//      amount.DATA 	金额 	1个币种符号+10位以内纯数字，可带小数，结尾可带“元” 	可带小数
//      phone_number.DATA 	电话 	17位以内，数字、符号 	电话号码，例：+86-0766-66888866
//      car_number.DATA 	车牌 	8位以内，第一位与最后一位可为汉字，其余为字母或数字 	车牌号码：粤A8Z888挂
//      name.DATA 	姓名 	10个以内纯汉字或20个以内纯字母或符号 	中文名10个汉字内；纯英文名20个字母内；中文和字母混合按中文名算，10个字内
//      phrase.DATA 	汉字 	5个以内汉字 	5个以内纯汉字，例如：配送中

		// 第一个内容： 订单号
		WxMaSubscribeMessage.Data wxMaSubscribeData1 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData1.setName("number1");
		wxMaSubscribeData1.setValue(order.getOrderSn());
		// 每个参数 存放到大集合中
		wxMaSubscribeData.add(wxMaSubscribeData1);
		// 第二个内容：订单类型
		WxMaSubscribeMessage.Data wxMaSubscribeData2 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData2.setName("thing11");
		wxMaSubscribeData2.setValue(findName(order.getJtType()));
		wxMaSubscribeData.add(wxMaSubscribeData2);

		// 第三个内容：预约地址
		WxMaSubscribeMessage.Data wxMaSubscribeData3 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData3.setName("thing10");
		wxMaSubscribeData3.setValue(order.getOutAddress());
		wxMaSubscribeData.add(wxMaSubscribeData3);

		// 第三个内容：收货地址
		WxMaSubscribeMessage.Data wxMaSubscribeData4 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData4.setName("thing3");
		wxMaSubscribeData4.setValue(order.getAddress());
		wxMaSubscribeData.add(wxMaSubscribeData4);

		// 第三个内容：订单描述
		WxMaSubscribeMessage.Data wxMaSubscribeData5 = new WxMaSubscribeMessage.Data();
		wxMaSubscribeData5.setName("thing9");
		/*
		if("".equals(order.getJtOutmessage())) {
			wxMaSubscribeData5.setValue("无");
		}else {
			wxMaSubscribeData5.setValue(order.getJtOutmessage());
		}*/
	/*
		wxMaSubscribeData5.setValue("已接单，极兔宝宝飞速送达中.");
		wxMaSubscribeData.add(wxMaSubscribeData5);

		// 把集合给大的data
		subscribeMessage.setData(wxMaSubscribeData);
		return subscribeMessage;
	}

	public String findName(Integer n) {
		if(n == 0) {
			return "代领快递";
		}
		if(n == 1) {
			return "代领外卖";
		}
		if(n == 2) {
			return "代买";
		}
		if(n == 3) {
			return "其他";
		}
		return null;
	}
	*/
}
