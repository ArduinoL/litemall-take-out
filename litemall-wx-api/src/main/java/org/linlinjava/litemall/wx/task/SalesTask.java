package org.linlinjava.litemall.wx.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.service.LitemallGoodsService;
import org.linlinjava.litemall.db.util.HomeCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.alibaba.fastjson.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Component
public class SalesTask{
    private final Log logger = LogFactory.getLog(SalesTask.class);
    

    @Autowired
    private LitemallGoodsService GoodsService;

    /**
     * 每隔30分钟执行
     */
    //@Scheduled(fixedDelay = 30 * 60 * 1000)
    public void checkCouponExpired() {
        logger.info("系统开启任务---修改商品销量--");
        /*        Calendar now = Calendar.getInstance();

        System.out.println("年：" + now.get(Calendar.YEAR));
 
        System.out.println("月：" + (now.get(Calendar.MONTH) + 1));
 
        System.out.println("日：" + now.get(Calendar.DAY_OF_MONTH));
 
        System.out.println("时：" + now.get(Calendar.HOUR_OF_DAY));
 
        System.out.println("分：" + now.get(Calendar.MINUTE));
 
        System.out.println("秒：" + now.get(Calendar.SECOND));
        

        Integer hour = now.get(Calendar.HOUR_OF_DAY);
        List<LitemallGoods> goodList = GoodsService.selectGoods();
        if(hour<24) {
        	for(int i=0;i<goodList.size();i++) {
        		Random random = new Random();
        		int m = random.nextInt(5);
        		Short mun = (short) m;
        		GoodsService.updateByGoodsIdSales(goodList.get(i).getId(), mun);
        	}
        }*/
        //优先从缓存中读取
        List<LitemallGoods> goodsList = new ArrayList<>();
        if (HomeCacheManager.hasGoodsData(HomeCacheManager.GOODSLIST)) {
        	goodsList = (List<LitemallGoods>) HomeCacheManager.getCacheData(HomeCacheManager.GOODSLIST).get("goodsList");
        	//String gs = JSONObject.parseObject(str).getString("goodsList");
        	//goodsList = JSONObject.parseArray(gs, LitemallGoods.class);
        	//System.out.println(goodsList);
		}else {
			//获取所有商品
			goodsList = GoodsService.selectGoods();
			Map<String, Object> data = new HashMap<String, Object>();
			data.put("goodsList", goodsList);
			//缓存数据
			HomeCacheManager.loadGoodsData(HomeCacheManager.GOODSLIST, data);
		}
        //List<LitemallGoods> goodList = GoodsService.selectGoods();
    	for(int i=0;i<goodsList.size();i++) {
    		Random random = new Random();
    		int m = random.nextInt(3);
    		//Short mun = (short) m;
    		goodsList.get(i).setSales(m);
    		//GoodsService.updateByGoodsIdSales(goodsList.get(i).getId(), mun);
    	}
    	GoodsService.updateByGoodsIdList(goodsList);
        logger.info("系统结束任务---修改商品销量--");
    }
}
