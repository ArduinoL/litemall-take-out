package org.linlinjava.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallOrder;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.service.LitemallSystemConfigService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.service.UserInfoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 用户服务
 */
@RestController
@RequestMapping("/wx/user")
@Validated
public class WxUserController {
    private final Log logger = LogFactory.getLog(WxUserController.class);

    @Autowired
    private LitemallOrderService orderService;
    @Autowired
    private UserInfoService userInfoService;
    @Autowired
    private LitemallSystemConfigService systemConfigService;

    /**
     * 用户个人页面数据
     * <p>
     * 目前是用户订单统计信息
     *
     * @param userId 用户ID
     * @return 用户个人页面数据
     */
    @GetMapping("index")
    public Object list(@LoginUser Integer userId,Integer shopId) {
        if (userId == null) {
            return ResponseUtil.unlogin();
        }

        Map<Object, Object> data = new HashMap<Object, Object>();
        //data.put("order", orderService.orderInfo(userId,shopId));
        //获取广告状态
		String BannerCenter = systemConfigService.getbannerCenter().get("litemall_wx_banner_center_A");
		boolean center = false;
		
		if(!StringUtils.isEmpty(BannerCenter)) {
			if(BannerCenter.equals("true")) {
				center = true;
			}
		}
        data.put("bannerOff", center);
        return ResponseUtil.ok(data);
    }
    
    @GetMapping("newUser")
    public Object newUser(@LoginUser Integer userId) {
        Map<Object, Object> data = new HashMap<Object, Object>();
        data.put("orderList", orderService.orderInfo(userId));
        return ResponseUtil.ok(data);
    }
    
    @GetMapping("getUserByuserId")
    public Object getUserByuserId(Integer userId) {
    	if(userId == null) {
    		return ResponseUtil.badArgument();
    	}
    	return ResponseUtil.ok(userInfoService.getInfo(userId));
    }

}