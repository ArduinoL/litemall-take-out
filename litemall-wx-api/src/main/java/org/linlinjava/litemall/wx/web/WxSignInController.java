package org.linlinjava.litemall.wx.web;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallUserSign;
import org.linlinjava.litemall.db.service.LitemallUserSignService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;




/**
 * 签到
 */
@RestController
@RequestMapping("/wx/sign")
@Validated
public class WxSignInController {
	private final Log logger = LogFactory.getLog(WxSignInController.class);

	@Autowired
	private LitemallUserSignService userSignService;

	@PostMapping("sign")
	public Object sign(@LoginUser Integer userId) {
		if(userId == null) {
			return ResponseUtil.unlogin();
		}
		Map<Object, Object> data = new HashMap<Object, Object>();
		boolean msg = false;
		//查询用户签到记录
		LitemallUserSign u = userSignService.getOneSignUser(userId);
		if(u == null) {
			LitemallUserSign us = new LitemallUserSign();
			us.setUserId(userId);
			us.setRecord("您已连续签到 1 天");
			us.setStage(1);
			us.setNumber(1);
			us.setGain(0);
			us.setHave(0);
			us.setAddTime(LocalDateTime.now());
			//创建用户签到记录
			userSignService.insertUserSign(us);
		}else {
			//用户昨天签到日期与现在日期对比
			long t = getDateNum(u);
			if(t == 0) {
				//今天已签到，无法再签到
				return ResponseUtil.custom("今天已签到!");
			}
			if(t == 1) {
				//签到
				int n = u.getNumber()+1;
				u.setRecord("您已连续签到 "+ n +"天");
				int num = u.getNumber()+1;
				u.setNumber(num);
				if(num == (u.getStage()*7)) {
					//获取奖励
					int g = u.getGain()+1;
					u.setGain(g);
					u.setNumber(0);
					u.setStage(u.getStage()+1);
					msg = true;
				}
				userSignService.updateByUserId(u);
			}
			if(t > 1) {
				//重新统计签到
				u.setNumber(1);
				u.setRecord("您已连续签到 1 天");
				userSignService.updateByUserId(u);
			}
		}
		data.put("msg", msg);
		return ResponseUtil.ok();
	}
	
	@GetMapping("getOneSignUser")
	public Object getUserSign(@LoginUser Integer userId) {
		if(userId == null) {
			return ResponseUtil.unlogin();
		}
		Map<Object, Object> data = new HashMap<Object, Object>();
		boolean isSign = false;//是否签到
		boolean bk = false;//是否断开
		int day = 0;//需要签到天数
		//int dot = 0;//已签到天数
		long t = 0;
		LitemallUserSign us = userSignService.getOneSignUser(userId);
		if(us == null) {
			isSign = false;
		}else {
			//需要签到天数
			day = (7*us.getStage());
			//判断今天能否签到
			t = getDateNum(us);
			//System.out.println("相差天数："+t);
			if(t == 0) {
				//已签到
				isSign = true;
			}else if(t > 1){
				isSign = false;
				bk = true;//已断开连续签到
			}
			data.put("us", us);
		}
		data.put("isSign", isSign);
		data.put("bk", bk);
		data.put("day", day);
		data.put("t", t);
		return ResponseUtil.ok(data);
	}
	
	/**
	 * 比较两个时间差，返回相差天数
	 * @param us
	 * @return
	 */
	public static long getDateNum(LitemallUserSign us) {
		LocalDate today = LocalDate.now();
		LocalDate localDate1 = LocalDate.of(us.getUpdateTime().getYear(), us.getUpdateTime().getMonth(), us.getUpdateTime().getDayOfMonth());
		LocalDate localDate2 = LocalDate.of(today.getYear(), today.getMonth(), today.getDayOfMonth());
		long t = localDate2.toEpochDay()-localDate1.toEpochDay();
		return t;
	}
	
	@GetMapping("getSignAward")
	public Object getSignAward(@LoginUser Integer userId) {
		if(userId == null) {
			return ResponseUtil.unlogin();
		}
		LitemallUserSign us = userSignService.getOneSignUser(userId);
		if(us.getHave() < us.getGain()) {
			return ResponseUtil.ok();
		}
		return ResponseUtil.custom("奖励已领完~");
	}
	
	
}