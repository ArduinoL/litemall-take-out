package org.linlinjava.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.redis.RedisCache;
import org.linlinjava.litemall.core.util.JsonUtil;
import org.linlinjava.litemall.core.util.RegexUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallAddress;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallShopSetting;
import org.linlinjava.litemall.db.service.LitemallAddressService;
import org.linlinjava.litemall.db.service.LitemallRegionService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.linlinjava.litemall.db.service.LitemallShopSettingService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.service.GetRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheConfig;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 自提店铺
 */
@RestController
@RequestMapping("/wx/shop")
@Validated
public class WxShopController extends GetRegionService {
	private final Log logger = LogFactory.getLog(WxShopController.class);

	@Autowired
	private LitemallShopService shopService;
	@Autowired
	private LitemallShopSettingService shopSettingService;
	@Autowired
	private RedisCache redisCache;


	/**
	 * 用户收货地址列表
	 *
	 * @param userId 用户ID
	 * @return 收货地址列表
	 */
	@GetMapping("list")
	public Object list() {
		List<LitemallShop> shopList =  shopService.querySelective(null, null, null, 1, 10);
		return ResponseUtil.okList(shopList);
	}
	
	/**
	 * 获取门店信息
	 * @param id
	 * @return
	 */
	@GetMapping("getOneByShopID")
	public Object getOneByShopID(Integer shopId) {
		if(shopId == null) {
			return ResponseUtil.badArgument();
		}
		String cacheKey = "shop_info_"+shopId;
		//读取缓存
		if(redisCache.hasData(cacheKey)) {
			String json = redisCache.getCacheObject(cacheKey);
			return ResponseUtil.ok(JsonUtil.jsonToObject(json));
		}
		
		LitemallShop shop = shopService.getOneByShopID(shopId);
		LitemallShopSetting shopSetting = shopSettingService.getOneSetting(shopId);
		
        Map<String, Object> result = new HashMap<>();
        result.put("shop", shop);
        result.put("shopSetting", shopSetting);
        redisCache.setCacheObject(cacheKey,JsonUtil.mapToJson(result));
		return ResponseUtil.ok(result);
	}
	/*
	@GetMapping("get_lng_lat")
	public Object get_lng_lat(String lng,String lat) {
		List<LitemallShop> shopList =  shopService.get_lng_lat(lng,lat,1,1);
		for(int i=0;i<shopList.size();i++) {
			Double m = Double.valueOf(shopList.get(i).getDistance());
			if(m < 1) {
				m=m*1000;
				shopList.get(i).setDistance(m+"m");
			}else {
				shopList.get(i).setDistance(m+"Km");
			}
		}
		return ResponseUtil.okList(shopList);
	}

	@GetMapping("getOneByShopID")
	public Object getOneByShopID(@NotNull Integer id) {
		LitemallShop shop = shopService.getOneByShopID(id);
		return ResponseUtil.ok(shop);
	}
	
	@GetMapping("getDistance")
	public Object getDistance(String lng,String lat) {
		LitemallShop shop = shopService.getDistance(lng, lat);
		if(shop != null) {
			Double dis = Double.valueOf(shop.getDistance());
			if(dis<10) {
				//在区域内
				return ResponseUtil.ok();
			}
		}
		return ResponseUtil.custom("您所在区域未开放此活动！");
	}
	
*/




}