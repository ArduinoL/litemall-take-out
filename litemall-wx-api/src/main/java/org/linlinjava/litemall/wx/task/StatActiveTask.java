package org.linlinjava.litemall.wx.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallActiveUser;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.service.LitemallActiveUserService;
import org.linlinjava.litemall.db.service.LitemallGoodsService;
import org.linlinjava.litemall.db.service.LitemallUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

@Component
public class StatActiveTask{
    private final Log logger = LogFactory.getLog(StatActiveTask.class);
    

    @Autowired
    private LitemallUserService userService;
    @Autowired
    private LitemallActiveUserService activeUserService;

    /**
     * 每天凌晨0点1分执行
     */
    @Scheduled(cron = "0 1 0 * * ?")
    public void checkCouponExpired() {
        logger.info("系统开启任务---统计当天用户在线人数--");
        int users = userService.countActiveUser();
        LitemallActiveUser ar = new LitemallActiveUser();
        ar.setUsers(users);
        activeUserService.insertActiveUser(ar);
        logger.info("系统结束任务---统计当天用户在线人数--");
    }
}
