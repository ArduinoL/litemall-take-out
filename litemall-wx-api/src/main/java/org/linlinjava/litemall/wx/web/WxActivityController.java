package org.linlinjava.litemall.wx.web;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.qcode.QCodeService;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallActivity;
import org.linlinjava.litemall.db.domain.LitemallActivityHelp;
import org.linlinjava.litemall.db.domain.LitemallUser;
import org.linlinjava.litemall.db.service.LitemallActivityHelpService;
import org.linlinjava.litemall.db.service.LitemallActivityService;
import org.linlinjava.litemall.db.service.LitemallOrderService;
import org.linlinjava.litemall.db.service.LitemallUserService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.service.WxPushService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


/**
 * 活动
 */
@RestController
@RequestMapping("/wx/activity")
@Validated
public class WxActivityController {
	private final Log logger = LogFactory.getLog(WxActivityController.class);

	
	@Autowired
	private LitemallActivityService activityService;
	@Autowired
	private LitemallActivityHelpService activityHelpService;
	@Autowired
	private LitemallUserService userService;
	@Autowired
	private QCodeService qCodeService;
    @Autowired
    private LitemallOrderService orderService;
    @Autowired
    private WxPushService wxPushService;

	
	/**
	 * 创建活动
	 * @param userId
	 * @return
	 */
	@PostMapping("save")
	public Object save(@LoginUser Integer userId) {
		if(userId == null) {
			return ResponseUtil.unlogin();
		}
		//检验用户是否已参加过活动
		LitemallActivity ay = activityService.getActivityByUserId(userId);
		LitemallActivity activity = new LitemallActivity();
		if(ay == null) {
			LitemallUser u = userService.findById(userId);
			activity.setName("免费领取水果");
			activity.setNumber(10);//10位新人助力可以领
			activity.setUserId(userId);
			activity.setStatus(0);
			activity.setAddNumber(0);
			activity.setAvatar(u.getAvatar());
			activity.setNickname(u.getNickname());
			activityService.insertActivity(activity);
		}else {
			return ResponseUtil.unalready();
		}
		Map<Object, Object> data = new HashMap<Object, Object>();
		data.put("ay", activity);
		return ResponseUtil.ok(data);
	}
	
	@GetMapping("getOneActivity")
	public Object getOneActivity(Integer userId) {
		if(userId == null) {
			return ResponseUtil.badArgument();
		}
		LitemallActivity ay = activityService.getActivityByUserId(userId);
		Map<Object, Object> data = new HashMap<Object, Object>();
		List<LitemallActivityHelp> ayHelpList = new ArrayList<>();
		int count = 0;
		if(ay != null) {
			//查询出助力记录
			ayHelpList = activityHelpService.getNumBerByactId(ay.getId());
			count = ayHelpList.size();
		}else {
			return ResponseUtil.badArgument();
		}
		//获得的奖励
		Integer getcount = activityService.getActivityByUserIdCount(userId);
		//查询是否是老用户
		data.put("orderList", orderService.orderInfo(userId));
		
		data.put("count", count);
		data.put("getcount", getcount);
        data.put("ayHelpList", ayHelpList);
        data.put("ay", ay);
        
        return ResponseUtil.ok(data);
	}
	
	@GetMapping("addNumber")
	public Object addNumber(@LoginUser Integer userId,Integer actId,String avatar) {
		if(userId == null) {
			return ResponseUtil.unlogin();
		}
		if(actId == null || StringUtils.isEmpty(avatar)) {
			return ResponseUtil.badArgumentValue();
		}
		//查询是不是自己助力自己
		LitemallActivity ay = activityService.getActivityByUserId(userId);
		if(ay != null) {
			if(ay.getId() == actId) {
				return ResponseUtil.custom("不能给自己助力，去邀请好友帮忙吧~");
			}
		}
		List<LitemallActivityHelp> helpList = activityHelpService.getCountByUserId(userId);
		if(helpList.size() > 0) {
			return ResponseUtil.custom("您已帮助好友助力过~");
		}
		LitemallActivityHelp ayHelpList = activityHelpService.getCountByUserIdAndActId(userId, actId);
		if(ayHelpList != null) {
			return ResponseUtil.custom("您已帮助好友助力过~");
		}
		//添加助力记录
		LitemallActivityHelp ayHelp = new LitemallActivityHelp();
		ayHelp.setAvatar(avatar);
		ayHelp.setUserId(userId);
		ayHelp.setActId(actId);
		activityHelpService.insertActivity(ayHelp);
		LitemallActivity neway = new LitemallActivity();
		neway.setId(actId);
		neway.setAddNumber(1);
		//查询是否已助力成功
		LitemallActivity newAy = activityService.getActivityById(actId);
		if((newAy.getNumber()-1) == newAy.getAddNumber()) {
			//助力成功
			//修改活动状态
			neway.setStatus(1);
			//推送给用户助力成功
			//查询用户 openid
			LitemallUser u = userService.findById(newAy.getUserId());
			wxPushService.notifyActivityUser(u.getWeixinOpenid());
		}
		//修改活动记录
		activityService.updateById(neway);
		
		return ResponseUtil.ok();
	}
	
	@GetMapping("shareImg")
	public Object url(Integer userId,Integer shopId) {
		if(userId == null || shopId == null) {
			return ResponseUtil.badArgument();
		}
		//查询用户活动是否有分享图
		LitemallActivity ay = activityService.getActivityByUserId(userId);
		if(ay != null) {
			if(!StringUtils.isEmpty(ay.getShareUrl())) {
				return ResponseUtil.ok(ay.getShareUrl());
			}
		}
		String ShareUrl = qCodeService.createNewQcode(userId, shopId);
		//保存
		//修改活动记录
		LitemallActivity neway = new LitemallActivity();
		neway.setId(ay.getId());
		neway.setShareUrl(ShareUrl);
		activityService.updateById(neway);
		return ResponseUtil.ok(qCodeService.createNewQcode(userId, shopId));
	}
	
	
	@GetMapping("getGoodsA")
	public Object getGoodsA(@LoginUser Integer userId) {
		if(userId == null) {
			return ResponseUtil.unlogin();
		}
		//检验是否可以领取商品
		Integer getCount = activityService.getGoodsA(userId);
		if(getCount > 0) {
			return ResponseUtil.ok();
		}
		return ResponseUtil.unauthz();
	}
	
	
	
	
	
	
}