package org.linlinjava.litemall.wx.task;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.db.domain.LitemallGoods;
import org.linlinjava.litemall.db.service.LitemallGoodsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.List;
import java.util.Random;

@Component
public class ZeroTask{
    private final Log logger = LogFactory.getLog(ZeroTask.class);
    
/*
    @Autowired
    private LitemallGoodsService GoodsService;*/

    /**
     * 每天凌晨0点执行
     */
    /*@Scheduled(cron = "0 0 0 * * ?")
    public void checkCouponExpired() {
        logger.info("系统开启任务---重置商品销量--");
        GoodsService.addSales(0);
        logger.info("系统结束任务---重置商品销量--");
    }*/
}
