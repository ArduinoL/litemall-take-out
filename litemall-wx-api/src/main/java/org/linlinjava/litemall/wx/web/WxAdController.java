package org.linlinjava.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.util.RegexUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallAd;
import org.linlinjava.litemall.db.domain.LitemallAddress;
import org.linlinjava.litemall.db.service.LitemallAdService;
import org.linlinjava.litemall.db.service.LitemallAddressService;
import org.linlinjava.litemall.db.service.LitemallRegionService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.service.GetRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 广告位
 * @author Administrator
 *
 */
@RestController
@RequestMapping("/wx/ad")
@Validated
public class WxAdController extends GetRegionService {
	private final Log logger = LogFactory.getLog(WxAdController.class);

    @Autowired
    private LitemallAdService adService;

	@GetMapping("list")
	public Object list(Integer position) {
		if (position == null) {
			return ResponseUtil.badArgument();
		}
		Byte p = (byte) position.intValue();
		List<LitemallAd> adList = adService.querySelective(p);
		return ResponseUtil.okList(adList);
	}

}