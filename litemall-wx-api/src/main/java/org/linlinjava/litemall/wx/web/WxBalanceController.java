package org.linlinjava.litemall.wx.web;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.linlinjava.litemall.core.util.RegexUtil;
import org.linlinjava.litemall.core.util.ResponseUtil;
import org.linlinjava.litemall.db.domain.LitemallAddress;
import org.linlinjava.litemall.db.domain.LitemallShop;
import org.linlinjava.litemall.db.domain.LitemallUserBalanceLog;
import org.linlinjava.litemall.db.service.LitemallAddressService;
import org.linlinjava.litemall.db.service.LitemallRegionService;
import org.linlinjava.litemall.db.service.LitemallShopService;
import org.linlinjava.litemall.db.service.LitemallUserBalanceLogService;
import org.linlinjava.litemall.wx.annotation.LoginUser;
import org.linlinjava.litemall.wx.service.GetRegionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户余额、余额日志
 */
@RestController
@RequestMapping("/wx/balance")
@Validated
public class WxBalanceController extends GetRegionService {
	private final Log logger = LogFactory.getLog(WxBalanceController.class);

	
	@Autowired
	private LitemallUserBalanceLogService balanceLogService;


	/**
	 * 用户收货地址列表
	 *
	 * @param userId 用户ID
	 * @return 收货地址列表
	 */
	@GetMapping("list")
	public Object list(Integer shopId,Integer showType) {
		if (shopId == null) {
			return ResponseUtil.unlogin();
		}
		List<LitemallUserBalanceLog> balanceLogList = balanceLogService.selectByShopIdList(shopId,showType);
		return ResponseUtil.okList(balanceLogList);
	}
	
}