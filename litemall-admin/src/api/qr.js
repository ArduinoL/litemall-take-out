import request from '@/utils/request'

export function listGroupQrcode(query) {
  return request({
    url: '/groupQrcode/list',
    method: 'get',
    params: query
  })
}

export function createAd(data) {
  return request({
    url: '/groupQrcode/create',
    method: 'post',
    data
  })
}

export function readAd(data) {
  return request({
    url: '/groupQrcode/read',
    method: 'get',
    data
  })
}

export function updateAd(data) {
  return request({
    url: '/groupQrcode/update',
    method: 'post',
    data
  })
}

export function deleteAd(data) {
  return request({
    url: '/groupQrcode/delete',
    method: 'post',
    data
  })
}
