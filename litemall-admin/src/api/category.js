import request from '@/utils/request'

export function listCategory(query) {
  return request({
    url: '/category/list',
    method: 'get',
    params: query
  })
}

export function listCategorySupport(query) {
  return request({
    url: '/categorysupport/list',
    method: 'get',
    params: query
  })
}

export function listCategorySupport2(query) {
  return request({
    url: '/categorysupport/list2',
    method: 'get',
    params: query
  })
}

export function listCatL1() {
  return request({
    url: '/category/l1',
    method: 'get'
  })
}

export function createCategory(data) {
  return request({
    url: '/category/create',
    method: 'post',
    data
  })
}

export function createCategorySupport(data) {
  return request({
    url: '/categorysupport/create',
    method: 'post',
    data
  })
}

export function readCategory(data) {
  return request({
    url: '/category/read',
    method: 'get',
    data
  })
}

export function updateCategory(data) {
  return request({
    url: '/category/update',
    method: 'post',
    data
  })
}

export function updateCategorySupport(data) {
  return request({
    url: '/categorysupport/update',
    method: 'post',
    data
  })
}

export function deleteCategory(data) {
  return request({
    url: '/category/delete',
    method: 'post',
    data
  })
}

export function deleteCategorySupport(data) {
  return request({
    url: '/categorysupport/delete',
    method: 'post',
    data
  })
}


export function listShopIds(data) {
  return request({
    url: '/categorysupport/selectShopIds',
    method: 'post',
    data
  })
}

export function listShop(query) {
  return request({
    url: '/categorysupport/selectShop',
    method: 'get',
    params: query
  })
}