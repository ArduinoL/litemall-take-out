import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/gzh/list',
    method: 'get',
    params: query
  })
}
export function syncWxUsers(data) {
  return request({
    url: '/gzh/syncWxUsers',
    method: 'post',
    data
  })
}

export function materialCount() {
  return request({
    url: '/wxAssets/list',
    method: 'get'
  })
}

export function materialFileBatchGet() {
  return request({
    url: '/wxAssets/materialFileBatchGet',
    method: 'get'
  })
}

export function materialNewsUpload(data) {
  return request({
    url: '/wxAssets/materialNewsUpload',
    method: 'post',
    data
  })
}