import request from '@/utils/request'

export function listShop(query) {
  return request({
    url: '/shop/list',
    method: 'get',
    params: query
  })
}
export function listMall(query) {
  return request({
    url: '/shop/shopIncome',
    method: 'get',
    params: query
  })
}
export function createShop(data) {
  return request({
    url: '/shop/create',
    method: 'post',
    data
  })
}

export function updateShop(data) {
  return request({
    url: '/shop/update',
    method: 'post',
    data
  })
}

export function updateShopOff(data) {
  return request({
    url: '/shop/updateShopOff',
    method: 'post',
    data
  })
}

export function deleteShop(data) {
  return request({
    url: '/shop/delete',
    method: 'post',
    data
  })
}

export function listAdmin(query) {
  return request({
    url: '/school/listAdmin',
    method: 'get',
    params: query
  })
}

export function createShopSetting(data) {
  return request({
    url: '/shopsetting/create',
    method: 'post',
    data
  })
}

export function updateShopSetting(data) {
  return request({
    url: '/shopsetting/update',
    method: 'post',
    data
  })
}

export function getOneSetting(ShopId) {
  return request({
    url: '/shopsetting/getOneSetting?ShopId=' + ShopId,
    method: 'get'
  })
}

export function listShopBalance(query) {
  return request({
    url: '/shop/shopIncome',
    method: 'get',
    params: query
  })
}

export function updateShopBound(data) {
  return request({
    url: '/shop/updateShopBound',
    method: 'post',
    data
  })
}
