import request from '@/utils/request'

export function getMenu(query) {
  return request({
    url: '/wxMenu/getMenu',
    method: 'get',
    params: query
  })
}
export function updateMenu(data) {
  return request({
    url: '/wxMenu/updateMenu',
    method: 'post',
    data
  })
}
