import request from '@/utils/request'

export function listSysUser(query) {
  return request({
    url: '/sysuser/list',
    method: 'get',
    params: query
  })
}
