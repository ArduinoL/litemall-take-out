import request from '@/utils/request'

export function listSchool(query) {
  return request({
    url: '/school/list',
    method: 'get',
    params: query
  })
}
export function createSchool(data) {
  return request({
    url: '/school/create',
    method: 'post',
    data
  })
}
export function updateSchool(data) {
  return request({
    url: '/school/update',
    method: 'post',
    data
  })
}
export function deleteSchool(data) {
  return request({
    url: '/school/delete',
    method: 'post',
    data
  })
}

export function listAdmin(query) {
  return request({
    url: '/school/listAdmin',
    method: 'get',
    params: query
  })
}

export function listShop(query) {
  return request({
    url: '/school/selectShop',
    method: 'get',
    params: query
  })
}

export function listShopIds2(ShopIds) {
  const data = {
    ShopIds
  }
  return request({
    url: '/school/selectShopIds',
    method: 'get',
    data
  })
}

export function listShopIds(data) {
  return request({
    url: '/school/selectShopIds',
    method: 'post',
    data
  })
}