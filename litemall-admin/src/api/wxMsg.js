import request from '@/utils/request'

export function fetchList(query) {
  return request({
    url: '/wxMsg/list',
    method: 'get',
    params: query
  })
}
export function save(data) {
  return request({
    url: '/wxMsg/save',
    method: 'post',
    data
  })
}

export function deleteMsg(data) {
  return request({
    url: '/wxMsg/delMsg',
    method: 'post',
    data
  })
}
